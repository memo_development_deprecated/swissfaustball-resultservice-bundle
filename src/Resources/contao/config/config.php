<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   ResultServiceBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
if (!isset($GLOBALS['BE_MOD']['mod_resultservice']))
{
    ArrayUtil::arrayInsert($GLOBALS['BE_MOD'], 2, array('mod_resultservice' => array()));
}


ArrayUtil::arrayInsert($GLOBALS['BE_MOD']['mod_resultservice'], 3, array
(
    'attributes' => array
    (
        'tables'       => array(
            'tl_resultservice_attributes',
            'tl_resultservice_log'
        ),
    ),
    'gamelog' => array
    (
        'tables'       => array(
            'tl_resultservice_log'
        )

    )

));


// Front end modules
$GLOBALS['FE_MOD']['memo_resultservice'] = array
(
    'memo_resultservice_entry'    => 'Memo\ResultServiceBundle\Module\ModuleResultServiceEntry',
    'memo_resultservice_reader'   => 'Memo\ResultServiceBundle\Module\ModuleResultServiceReader'
);

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_resultservice_attributes']        = 'Memo\ResultServiceBundle\Model\ResultserviceAttributesModel';
$GLOBALS['TL_MODELS']['tl_resultservice_log']               = 'Memo\ResultServiceBundle\Model\ResultserviceLogModel';

// Add permissions
$GLOBALS['TL_PERMISSIONS'][] = 'memoresultservice';

/**
 * Backend CSS
 */
if(TL_MODE == 'BE')
{
    $GLOBALS['TL_CSS'][]        = '/bundles/resultservice/css/backend.css|static';
}

?>
