<?PHP


$GLOBALS['TL_DCA']['tl_resultservice_log'] = [
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'notEditable' => true,
        'notDeletable' => true,
        'notCreatable' => true,
        'notCopyable'  => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'id_game,id_team' => 'index',
                'skey' => 'index'
            ],
        ],
    ],
    'list' => [
        'sorting' => [
            'mode'      => 2,
            'fields'    =>  ['id_game'],'headerFields'=>[],
            'panelLayout' => 'filter;sort,search,limit',

        ],
        'label' => [
            'fields'=>[
                'tstamp',
                'id_game',
                'playset',
                'score',
                'player',
                'skey',
                'svalue',
                'team'
            ],
            'showColumns' => true,
            'label_callback' => ['tl_resultservice_log_ext','createListLable']
        ]
        ],
    // Palettes
	'palettes' => array
	(
		'__selector__'                => array(''),
		'default'                     => '{log_legend},id_game, playset, score, player,skey,svalue,table;'
	),
    'fields' => [
            'id' => array(
		      'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		    ),
		    'tstamp' => array(
                'label'     => ['Zeitstempel'],
                'sorting'   => true,
                'flag'      => 12,
                'inputType' => 'text',
                'sql'       => "int(10) unsigned NOT NULL default '0'"
		    ),
            'id_game' => array(
                'label'     => ['Spiel'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'filter'    => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'digit'),
                'sql'   => "int(11) unsigned NOT NULL default 0"
            ),
            'id_team' => array(
                'label'     => ['Team ID'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'digit'),
                'sql'   => "int(11) unsigned NOT NULL default 0"
            ),
            'playset' => array(
                'label'     => ['Spiel Satz'],
                'inputType' => 'text',
                'eval'      => array('mandatory'=>false, 'maxlength'=>1, 'tl_class'=>'w50','rgxp'=>'digit'),
                'sql' => "int(1) unsigned NOT NULL default 1"
            ),
            'score' => array(
                'label'     => ['Spielstand'],
                'inputType' => 'text',
                'eval'      => array('mandatory'=>false, 'tl_class'=>'w50'),
                'sql' => "varchar(255) NOT NULL default ''"
            ),
            'player' => array(
                'label'     => ['Spieler'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=>'w50'),
                'sql' => "blob NULL"
            ),
            'team' => array(
                'label'     => ['Team'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=>'w50'),
                'sql' => "blob NULL"
            ),
            'table' => array(
                'label'     => ['Tabelle'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=>'w50'),
                'sql' => "varchar(255) NOT NULL default ''"
            ),
            'skey' => array(
                'label'     => ['Feld'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=> 'w50'),
                'sql' => "varchar(255) NOT NULL default ''"
            ),
            'svalue' => array(
                'label'     => ['Wert'],
                'inputType' => 'text',
                'search'    => true,
                'sorting'   => true,
                'eval'      => array('mandatory'=>false, 'tl_class'=> 'w50'),
                'sql' => "blob NULL"
            ),
        ]

];

use Memo\ModSwissfaustballBundle\Model\SfGamesModel;
use Memo\ModClubBundle\Model\TeamModel;
use Memo\ModClubBundle\Model\ClubModel;

class tl_resultservice_log_ext extends Backend
{
    /**
     * Create Liste Items by Relation Table and Date-Formats
     */
    public function createListLable(array $row, string $label, DataContainer $dc, array $labels): array
    {
        
        //Get Game Infoa
        $oGame   = SfGamesModel::findByPk($row['id_game']);

        //Get Club + Team Info
        $oTeam   = TeamModel::findByPk($row['id_team']);
        $oClub   = ClubModel::findBy('id',$oTeam->pid);

        //Get Player Data
        $aPlayer = unserialize($row['player']);

        //Get List Fields and there Positions
        $fields  = $GLOBALS['TL_DCA'][$dc->table]['list']['label']['fields'];
        $keyTime = array_search('tstamp', $fields, true);
        $keyGame = array_search('id_game', $fields, true);
        $keyPlayer = array_search('player', $fields, true);
        $keyTeam = array_search('team', $fields, true);

        $labels[$keyTime] = date("d.m.y H:i:s",$row['tstamp']);
        $labels[$keyGame] = sprintf('<div>%s</div><div style="margin-top:5px;">%s</div>',$oGame->round,$oGame->category);
        $labels[$keyPlayer] = empty($aPlayer)? '&mdash;' : sprintf("%s %s (%s)",$aPlayer['firstname'],$aPlayer['name'],$aPlayer['number']);
        $labels[$keyTeam] = $oClub->name;
        return $labels;
    }
}


?>