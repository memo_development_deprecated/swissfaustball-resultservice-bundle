<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

    PaletteManipulator::create()
        ->addLegend('info_legend', 'results_legend', PaletteManipulator::POSITION_BEFORE)
        ->addField('viewers', 'info_legend', PaletteManipulator::POSITION_APPEND)
        ->addField('attributes', 'info_legend', PaletteManipulator::POSITION_APPEND)
        ->applyToPalette('default', 'tl_sf_games');

    PaletteManipulator::create()
        ->addField('rsclosed', 'results_legend', PaletteManipulator::POSITION_APPEND)
        ->applyToPalette('default', 'tl_sf_games');

$GLOBALS['TL_DCA']['tl_sf_games']['palettes']['default'] = str_replace(',team_a,',
            ',team_a,team_a_trainer,team_a_supervisor,',
        $GLOBALS['TL_DCA']['tl_sf_games']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_sf_games']['palettes']['default'] = str_replace(',team_b,',
    ',team_b,team_b_trainer,team_b_supervisor,',
    $GLOBALS['TL_DCA']['tl_sf_games']['palettes']['default']);


$GLOBALS['TL_DCA']['tl_sf_games']['fields']['team_a']['eval']['tl_class'] = 'clr';
$GLOBALS['TL_DCA']['tl_sf_games']['fields']['team_b']['eval']['tl_class'] = 'clr';

$GLOBALS['TL_DCA']['tl_sf_games']['fields']['viewers'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_sf_games']['viewers'],
    'exclude'                 => true,
    'filter'				  => false,
    'sorting'				  => false,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'maxlength'=>4, 'tl_class'=>'w50','rgxp'=>'digit'),
    'sql'                     => "int(5) unsigned NOT NULL default '0'"
);
$GLOBALS['TL_DCA']['tl_sf_games']['fields']['attributes'] = array(
    'label'                     =>      &$GLOBALS['TL_LANG']['tl_sf_games']['attributes'],
    'inputType'               => 'select',
    'foreignKey'              => 'tl_resultservice_attributes.CONCAT(tl_resultservice_attributes.title, " (", tl_resultservice_attributes.type,")")',
    'eval'                    => array('mandatory'=>false, 'chosen'=>true,'chosen'=>true,'multiple'=>true, 'tl_class' => 'w50', 'includeBlankOption' => true),
    'sql'                     => "text NULL",
    'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
);
$GLOBALS['TL_DCA']['tl_sf_games']['fields']['rsclosed'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_sf_games']['rsclosed'],
    'exclude'                 => true,
    'filter'				  => true,
    'sorting'				  => true,
    'default'				  => 0,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'clr'),
    'sql'                     => "char(1) default 0"
);


$GLOBALS['TL_DCA']['tl_sf_games']['fields']['team_a_trainer'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_sf_games']['team_a_trainer'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false,  'readonly'=>true, 'tl_class'=>'w50 clr'),
    'sql'                     => "varchar(256) default ''"
);
$GLOBALS['TL_DCA']['tl_sf_games']['fields']['team_a_supervisor'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_sf_games']['team_a_supervisor'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'readonly'=>true, 'tl_class'=>'w50'),
    'sql'                     => "varchar(256) default ''"
);
$GLOBALS['TL_DCA']['tl_sf_games']['fields']['team_b_trainer'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_sf_games']['team_b_trainer'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'readonly'=>true, 'tl_class'=>'w50 clr'),
    'sql'                     => "varchar(256) default ''"
);
$GLOBALS['TL_DCA']['tl_sf_games']['fields']['team_b_supervisor'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_sf_games']['team_b_supervisor'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'readonly'=>true, 'tl_class'=>'w50'),
    'sql'                     => "varchar(256) default ''"
);

/**
 * add operation
 */
$GLOBALS['TL_DCA']['tl_sf_games']['list']['operations']['resultService'] = array
(
    'label'					=> &$GLOBALS['TL_LANG']['tl_sf_games']['resultService'],
    'href'					=> 'key=resultService',
    'icon'					=> '/bundles/resultservice/img/clipboard_result_icon.svg',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'		=> array('tl_sf_games_result', 'resultServiceButton')
);



class tl_sf_games_result extends tl_sf_games
{
    /**
     * Add Button to result service
     * @param $row
     * @return string
     */
    public function resultServiceButton($row,$href, $label, $title, $icon, $attributes, $table){
        if($row['round_date']+7200 > time()) {
            return false;
        }elseif($row['rsclosed'] == 1) {
            return sprintf('<a href="/ResultServices/createReceipt/%s" target="_blank" class="memoIcons" title="Resultateblatt öffnen">%s</a>', $row['id'], Image::getHtml('/bundles/resultservice/img/document_export_icon.svg', $label));
        }else{
            return sprintf('<a href="/resultateservice.html?gid=%s" target="_blank"  class="memoIcons" title="Resultateservice öffnen">%s</a>', $row['id'], Image::getHtml($icon, $label));
        }
    }
}
