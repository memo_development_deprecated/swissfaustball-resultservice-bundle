<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ResultServiceBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_resultservice_attributes
 */
$GLOBALS['TL_DCA']['tl_resultservice_attributes'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'			=> true,

        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 0,
            'flag'                    => 11,
            'fields'                  => array('title','sorting'),
            'panelLayout'             => 'sort;filter;search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'label_callback'   => array('tl_resultservice_attributes', 'addType')

        ),

        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['toggle'],
                'icon'                => 'visible.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_resultservice_attributes', 'toggleIcon')
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif'
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{attributes_legend}, title, alias, type, published;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        ''                            => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'sorting' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['MSC']['sorting'],
            'sorting'                 => true,
            'flag'                    => 11,
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'               	  => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['title'],
            'exclude'             	  => true,
            'filter'              	  => false,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'           	  => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength' => 255, 'tl_class' => 'clr w50'),
            'sql'                 	  => "varchar(255) NOT NULL default ''",
        ),
        'alias' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['alias'],
            'exclude'                 => true,
            'filter'                  => false,
            'search'                  => true,
            'sorting'                 => false,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'alias', 'doNotCopy'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
            'save_callback' => array
            (
                array('tl_resultservice_attributes', 'generateAlias')
            ),
            'sql'                     => "varchar(128) COLLATE utf8_bin NOT NULL default ''"
        ),
        'type' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['type'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'options'                 => array('Wetter', 'Terrain'),
            'eval' => array('multiple' => false,'includeBlankOption'=>false,'chosen'=>true,'tl_class'=>'clr'),
            'sql'                     => "varchar(255) NULL"
        ),
        'published' => array
        (
            'label'               	  => &$GLOBALS['TL_LANG']['tl_resultservice_attributes']['published'],
            'exclude'             	  => true,
            'filter'              	  => true,
            'search'                  => false,
            'sorting'                 => false,
            'inputType'           	  => 'checkbox',
            'eval'                    => array('doNotCopy'=>true, 'tl_class'=>'w50'),
            'sql'                 	  => "char(1) NOT NULL default ''",
        ),
    )
);



use \Memo\ResultServiceBundle\Model\ResultserviceAttributesModel;

/**
 * Class tl_mod_club
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_resultservice_attributes extends Backend
{

    /**
     * Auto-generate an article alias if it has not been set yet
     *
     * @param mixed         $varValue
     * @param DataContainer $dc
     *
     * @return string
     *
     * @throws Exception
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;
        // Generate an alias if there is none
        if ($varValue == '')
        {
            $autoAlias = true;
            $varValue = StringUtil::generateAlias($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare("SELECT id FROM tl_resultservice_attributes WHERE id=? OR alias=?")
            ->execute($dc->id, $varValue);
        // Check whether the page alias exists
        if ($objAlias->numRows > 1)
        {
            if (!$autoAlias)
            {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }
            $varValue .= '-' . $dc->id;
        }
        return $varValue;
    }


    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid')))
        {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);
        if (!$row['published'])
        {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"').'</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }


        // Update the database
        $objAttrModel = ResultserviceAttributesModel::findOneById($intId);
        $objAttrModel->published = ($blnVisible ? '1' : '');
        $objAttrModel->save();
    }

    function addType($row,$label){
        return $label . " <small style='color:#999999;'>(".$row['type'].")</small>";
    }
}

?>
