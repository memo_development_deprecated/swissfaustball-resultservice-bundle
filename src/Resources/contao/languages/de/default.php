<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   MEMO
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['MOD']['attributes'] = array('Attribute', '');
$GLOBALS['TL_LANG']['MOD']['gamelog'] = array('Spiel Log', '');

$GLOBALS['TL_LANG']['FMD']['memo_resultservice'] 			= "Swiss Faustball — Resultateservice";
$GLOBALS['TL_LANG']['FMD']['memo_resultservice_entry'] 		= "Resultateservice Eingabe";
$GLOBALS['TL_LANG']['FMD']['memo_resultservice_reader'] 		= "Resultateservice Reader";


?>