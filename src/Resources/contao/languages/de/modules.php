<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   ModClubBundle
 * @author    Michael Spiegel, Media Motion AG
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['mod_resultservice'] 		= array('Resultateservice', '');
$GLOBALS['TL_LANG']['MOD']['mod_resultservice']['attributes'] 		= array('Attribute', '');
$GLOBALS['TL_LANG']['MOD']['mod_resultservice']['gamelog'] 		= array('Spiel Log', '');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['memo_result_service_entry'] 	= array('Eingabe');
$GLOBALS['TL_LANG']['FMD']['memo_result_service_reader'] 	= array('Resultate Service Reader');
