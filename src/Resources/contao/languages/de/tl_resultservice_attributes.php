<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   ModClubBundle
 * @author    Michael Spiegel, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['attributes_legend']         = "Attribute";
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['title']                     = ["Attribut Titel",""];
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['aliase']                     = ["Attribut Alias",""];
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['type']                     = ["Attribut Typ",""];
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['new']   					= array('Neuer Eintrag', 'Neuen Eintrag anlegen');
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['edit']						= array('Eintrag bearbeiten ', 'Personen für diesen Eintrag bearbeiten');
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['cut']    					= array('Eintrag Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['copy']  					= array('Eintrag Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['delete']					= array('Eintrag Löschen ', 'ID %s löschen');
$GLOBALS['TL_LANG']['tl_resultservice_attributes']['toggle']					= array('Eintrag veröffentlichen ', 'ID %s veröffentlichen / verbergen');

?>