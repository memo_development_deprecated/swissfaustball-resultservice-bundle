<?php

/**
* Contao Open Source CMS
*
 * Copyright (c) 2005-2014 Leo Feyer
*
 * @package   MEMO
* @author    Christian Nussbaumer, Media Motion AG
* @license   MEMO
* @copyright Media Motion AG
*/


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_sf_games']['team_a_trainer'] = array('Trainer Team A', '');
$GLOBALS['TL_LANG']['tl_sf_games']['team_a_supervisor'] = array('Betreuer Team A', '');
$GLOBALS['TL_LANG']['tl_sf_games']['team_b_trainer'] = array('Trainer Team B', '');
$GLOBALS['TL_LANG']['tl_sf_games']['team_b_supervisor'] = array('Betreuer Team B', '');

$GLOBALS['TL_LANG']['tl_sf_games']['f_referee'] = array('Schiedsrichter', 'Optional kann hier ein Schiedsrichter gewählt werden, welcher dann im Spielbericht ausgegeben wird.');
$GLOBALS['TL_LANG']['tl_sf_games']['f_linesman'] = array('Linienrichter', 'Optional kann hier ein Linienrichter gewählt werden, welcher dann im Spielbericht ausgegeben wird.');
$GLOBALS['TL_LANG']['tl_sf_games']['f_linesman_2'] = array('Linienrichter 2', 'Optional kann hier ein Linienrichter 2 gewählt werden, welcher dann im Spielbericht ausgegeben wird.');
$GLOBALS['TL_LANG']['tl_sf_games']['f_writer'] = array('Anschreiber', 'Optional kann hier ein Anschreiber gewählt werden, welcher dann im Spielbericht ausgegeben wird.');
$GLOBALS['TL_LANG']['tl_sf_games']['attributes'] = array('Bedinungen', 'Wetter/Terrain');
$GLOBALS['TL_LANG']['tl_sf_games']['viewers'] = array('Anzahl Zuschauer', '');
$GLOBALS['TL_LANG']['tl_sf_games']['rsclosed'] = array('Resultate Service Eingabe abgeschlossen?', '');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_sf_games']['info_legend'] = 'Spiel-Info';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_sf_games']['gamereport'] = array('Spielbericht ', 'Spielbericht für die ID %s generieren');
$GLOBALS['TL_LANG']['tl_sf_games']['gameResultReport'] = array('Spielbericht ', 'Spielbericht für die ID %s generieren');
