(function ( $ ) {

    $.memoStopwatch = function(el,options) {
        var defaults = {
            autostart: false,
            startTime: 0
        };

        var plugin = this;
        var time = 0;
        var hour = Math.floor(time/10/60/60)%24;
        var mins = Math.floor(time/10/60)%60;
        var secs = Math.floor(time/10)%60;
        var tenths = time%10;
        var cycle;
        var running = 0;

        plugin.settings = {};

        var $element = $(element), // reference to the jQuery version of DOM element
            element = element; // reference to the actual DOM element

        plugin.settings = $.extend({}, defaults, options);

        plugin.init = function() {
            $(el).text(getTimerTime());
            if (plugin.settings.autostart === true) {
                plugin.start();
            }
        };

        plugin.start = function(){
            cycle = setTimeout(timerCycle, 1000);
            running = 1;
        };

        plugin.stop = function(){
            running = 0;
        };

        plugin.reset = function(){
            running = 0;
            time = 0;
            $(el).text('00:00:00');
        };


        var timerCycle = function () {
            time++;
            $(el).text(getTimerTime());

            if (running) {
                setTimeout(timerCycle, 1000);
            }
        };

        var getTimerTime = function()
        {

            if (plugin.settings.startTime > 0) {
                var startTime = new Date(parseInt(plugin.settings.startTime) * 1000);
                var now = new Date();
                var diff = now.getTime() - startTime.getTime();
                time = (parseInt(diff / 1000));
            }

            let hour = Math.floor(time / 60 / 60) % 24;
            let mins = Math.floor(time / 60) % 60;
            let secs = Math.floor(time ) % 60;

            if (hour < 10) {
                hour = "0" + hour;
            }
            if (mins < 10) {
                mins = "0" + mins;
            }
            if (secs < 10) {
                secs = "0" + secs;
            }
            return hour + ":" + mins + ":" + secs;
        };

        plugin.init();
    };

    $.fn.memoStopwatch = function(options) {
        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('memoStopwatch')) {
                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.memoStopwatch(this, options);
                $(this).data('memoStopwatch', plugin);
            }
        });

    };

}( jQuery ));
