'use strict';
import {Modal} from 'bootstrap';
import {Alert} from "bootstrap";
import {Tooltip} from "bootstrap";
import {Tab} from "bootstrap";
import SignaturePad from "signature_pad";

require('webpack-jquery-ui/autocomplete');
require('webpack-jquery-ui/datepicker');
require('webpack-jquery-ui/css');

var netStatusTimer = null;

$(document).ready(function(){
   $("a[data-modal-player]").on('click',function(e){
      if(!_checkOnlineStatus()) {
         return false;
      }
      var pID = $(this).data('modal-player');

      //show Spinner
      if($("#spinner").length) {$("#spinner").fadeIn();}
      if($("#modalbox").length) {$("#modalbox").remove();}

      //Get Modal Content
      $.post('/ResultServices/player/edit/'+pID,{playerID:pID},function(data){
         var modalContent = $(data);
         $("body").append(modalContent).ready(function(){{
            $("#spinner").hide();
            var editModal = new Modal(document.getElementById('modalbox'), {
               keyboard: false
            });
            editModal.show();

            //Add Submit Action
            $("form#frmEditPlayer",$("#modalbox")).on('submit',function(e){
               e.preventDefault();
               var frmData = $(this).serialize();
               $.post($(this).attr('action'),frmData,function(data){
                  var objReturn = jQuery.parseJSON(data);

                  let sBirthDateYear = new Date(objReturn.birthdate*1000);

                  $("[data-modal-player='" + objReturn.id +"']").text(objReturn.firstname+" "+objReturn.name);
                  $("[data-player-birthdate='" + objReturn.id +"']").text(sBirthDateYear.getFullYear());
                  $("[data-player_number='" + objReturn.id +"']").text(objReturn.number);
               });
               editModal.hide();
               return false;
            });

         }});
      });
   });

   /**
    * Modal Box onLoad
    */
   if($("#modalbox").length)
   {
      var show = $("#modalbox").data('show');
      if(show == 'onload')
      {
         var cntModal = new Modal(document.getElementById('modalbox'), {
            keyboard: false
         });
         cntModal.show();
      }
   };

   /**
    * Modal Delete Point OK Button
    */
   var deletePointModal;
   $("#frmDeletePoint").on('submit',function(e){
      e.preventDefault();
      $.post($(this).attr('action'), function (data) {
         var objReturn = jQuery.parseJSON(data);
         if (objReturn.score) {
            $("div.big-result").text(objReturn.score);
            reloadLiveScore();
            checkSetStatus();
            checkTimeOutButtonState();
            deletePointModal.hide();
         }
      });
      return false;
   });

   /**
    * Async
    * change Viewers Count
    */
   $("input#gameViewers").on('change',function(e){
      if(!_checkOnlineStatus()) {
         return false;
      }
      var gID = $("input#gameViewers").data('gameid');
      if(parseInt(gID)) {
         $.post('/ResultServices/viewers/change/' + gID, {gameId: gID,'viewers':parseInt($("input#gameViewers").val())}, function (data) {

         });
      }
   });

   /**
    * Async save Game Attributes on change
    */
   $(".game-attributes input[type='checkbox']").on('change',function(e){
      if(!_checkOnlineStatus()) {
         return false;
      }
      var gID = $("input#gameViewers").data('gameid');
      var element = $(this);

      if(parseInt(gID)) {
         $.post('/ResultServices/attributes/change/' + gID, {gameId: gID,'attrId':$(element).val(),'status':$(element).prop("checked")}, function (data) {

         });
      }
   });

   $("input[data-player-einsatz]").on('click',function(e){
      var pTable = $(this).closest('table');
      if(!_checkOnlineStatus()) {
         return false;
      }

      if($('input:checkbox:checked',pTable).length > 5){
         _createAlert('Max. 5 Spieler können gleichzeitig im Einsatz sein.','alert-warning');
         return false;
      }else{
         $.post('/ResultServices/player/status/',{'status':$(this).prop("checked"),'playerId':$(this).data('player-einsatz'),'gameId':$("input#gameViewers").data('gameid')});
      }

   });

   $("input[data-player-isCaptain]").on('click',function(e){
      if(!_checkOnlineStatus()) {
         return false;
      }
      $.post('/ResultServices/setCaptain/',{'status':$(this).prop("checked"),'playerId':$(this).val(),'gameId':$("input#gameViewers").data('gameid')});
   });

   //Officials Input Fields Autocomplete and async save
   $("input[data-officials]").autocomplete({
      source: "/ResultServices/game/getOfficials",
      minLength: 3
   });
   $("input[data-officials]").on("blur",function(e){
      e.preventDefault();
      if($(this).val().trim() != '') {
         $.post('/ResultServices/game/setOfficials', {
            gameId: $(".game-history").data('game'),
            type: $(this).data('officials'),
            value: $(this).val()
         });
      }
   });

   //get Staff | trainer/supervisor autocomplete
   $("input[data-staff]").autocomplete({
      source: function(request, response) {
         $.post("/ResultServices/game/getTeamStaff",{ term : request.term,tid:this.element.data('team'),gid:$(".game-history").data('game')},function(data){
            response(data);
         });
      },
      minLength: 3
   });

   //Save Staff | trainer/supervisor
   $("input[data-staff]").on("change",function(e){
      if(!_checkOnlineStatus()) {
         return false;
      }
      e.preventDefault();
      $.post('/ResultServices/game/setTeamStaff',{gameId:$(".game-history").data('game'),tid:$(this).data('team'),type:$(this).data('staff'),value:$(this).val()},function(){

      });
   });

   /**
    * Create New Player
    */
   $("a.player-create").on('click',function(e){
      if(!_checkOnlineStatus()) {
         return false;
      }
      e.preventDefault();
      //show Spinner
      if ($("#spinner").length) {$("#spinner").fadeIn();}
      //remove exist Modalboxes
      if ($("#modalbox").length) {$("#modalbox").remove();}

      //Get Modal Content
      $.post($(this).attr('href'), {
         teamId: $(this).data('team')
      }, function (data) {
         var modalContent = $(data);
         $("body").append(modalContent).ready(function () {
            $("#spinner").hide();
            var editModal = new Modal(document.getElementById('modalbox'), {
               keyboard: false
            });
            editModal.show();

            //Add Submit Action
            $("form#frmCreatePlayer", $("#modalbox")).on('submit', function (e) {
               e.preventDefault();
               $.post($(this).attr('action'), $(this).serialize(), function (data) {
                  var objReturn = jQuery.parseJSON(data);
               });
               editModal.hide();
               setTimeout(function(){document.location.reload();},100);
               return false;
            });
         });
      });
   });



   /**
    * 2. Spielverlauf
    */
   var tabEl = document.querySelector('button#game-tab');
   tabEl.addEventListener('shown.bs.tab', function (e) {

      //validate team player set
      var iTeamACount = $(".team-table-a input[data-player-einsatz]:checked").length;
      var iTeamBCount = $(".team-table-b input[data-player-einsatz]:checked").length;

      if(iTeamACount < 5 || iTeamBCount < 5)
      {
         $("button#admin-tab").click();
         _createAlert('Bitte wählen Sie 5 Spieler pro Manschaft für den Einsatz');
      }else{
         if($("#alert").length){$("#alert").remove();}
      }


   });


function _setButtonActions() {

      $('input[name="pass"]').on('click', function (e) {
         if(!_checkOnlineStatus()) {
            return false;
         }
         if ($(this).prop('checked')) {
            $.post('/ResultServices/setFirstBall/' + $(this).data('game'), {teamId: $(this).val()});
         }
      });

      /**
       * Einsprache Modal View
       */
      $("#btn-objection").on('click', function (e) {
         if(!_checkOnlineStatus()) {
            return false;
         }
         //show Spinner
         if ($("#spinner").length) {$("#spinner").fadeIn();}
         //remove exist Modalboxes
         if ($("#modalbox").length) {$("#modalbox").remove();}

         //Get Modal Content
         $.post('/ResultServices/objection/', {
            gid: $("#game").data('game'),
            'clubs': $(this).data('club')
         }, function (data) {
            var modalContent = $(data);
            $("body").append(modalContent).ready(function () {
               $("#spinner").hide();
               var editModal = new Modal(document.getElementById('modalbox'), {
                  keyboard: false
               });
               editModal.show();

               //Add Submit Action
               $("form#frmEditObjection", $("#modalbox")).on('submit', function (e) {
                  e.preventDefault();
                  let postData = $(this).serialize();
                  $.post($(this).attr('action'), postData, function (data) {
                     var objReturn = jQuery.parseJSON(data);
                  });
                  editModal.hide();
                  return false;
               });
            });
         });
      });

      /**
       * Einsprache Modal Vorfall
       */
      $("#btn-incident").on('click', function (e) {
         if(!_checkOnlineStatus()) {
            return false;
         }
         //show Spinner
         if ($("#spinner").length) {$("#spinner").fadeIn();}
         //remove exist Modalboxes
         if ($("#modalbox").length) {$("#modalbox").remove();}

         //Get Modal Content
         $.post('/ResultServices/incident/', {
            gid: $("#game").data('game')
         }, function (data) {
            var modalContent = $(data);
            $("body").append(modalContent).ready(function () {
               $("#spinner").hide();
               var editModal = new Modal(document.getElementById('modalbox'), {
                  keyboard: false
               });
               editModal.show();

               //Add Submit Action
               $("form#frmEditIncident", $("#modalbox")).on('submit', function (e) {
                  e.preventDefault();
                     //Validate Incident
                     if($('#incidentSelect').val() == '') {
                        $("#incidentSelect").addClass('border-sfbred');
                        return false;
                     }

                     $.post($(this).attr('action'), $(this).serialize(), function (data) {
                        var objReturn = jQuery.parseJSON(data);
                     });
                     editModal.hide();
                  return false;
               });
            });
         });
      });

      /**
       * Verwarnung Modal View
       */
      $("#btn-warn").on('click', function (e) {
         if(!_checkOnlineStatus()) {
            return false;
         }
         //show Spinner
         if ($("#spinner").length) {$("#spinner").fadeIn();}
         //remove exist Modalboxes
         if ($("#modalbox").length) {$("#modalbox").remove();}

         //Get Modal Content
         $.post('/ResultServices/warning/', {
            gid: $("#game").data('game'),
            'clubs': $(this).data('club')
         }, function (data) {
            var modalContent = $(data);
            $("body").append(modalContent).ready(function () {
               $("#spinner").hide();
               var editModal = new Modal(document.getElementById('modalbox'), {
                  keyboard: false
               });
               editModal.show();

               //Add Submit Action
               $("form#frmEditWarning", $("#modalbox")).on('submit', function (e) {
                  e.preventDefault();
                  let postData = $(this).serialize();
                  $.post($(this).attr('action'), postData, function (data) {
                     var objReturn = jQuery.parseJSON(data);
                     if ($("td[data-warning='" + objReturn.playerid + "']").length)
                     {
                        let msg = $('<a className="block" data-bs-toggle="tooltip" data-bs-placement="left" id="tt'+objReturn.playerid+'" title="" data-bs-original-title="'+objReturn.msg+'">X</a>');
                        $("td[data-warning='"+objReturn.playerid+"']").html(msg);
                        var tooltip = new Tooltip(document.getElementById('tt'+objReturn.playerid));
                     }
                  });
                  editModal.hide();
                  return false;
               });
            });
         });
      });



      /**
       * Spieler Wechsel
       */
      $("#btnChangeA,#btnChangeB").on('click', function (e) {
         e.preventDefault();
         if(!_checkOnlineStatus()) {
            return false;
         }
         //show Spinner
         if ($("#spinner").length) {
            $("#spinner").fadeIn();
         }

         //remove exist Modalboxes
         if ($("#modalbox").length) {
            $("#modalbox").remove();
         }

         /**
          * Get Modal Content Player Change
          */
         $.post('/ResultServices/player/change/' + $(this).data('game'), {
            gid: $(this).data('game'),
            'teamId': $(this).data('team')
         }, function (data) {
            var modalContent = $(data);
            $("body").append(modalContent).ready(function () {
               $("#spinner").hide();
               var editModal = new Modal(document.getElementById('modalbox'), {
                  keyboard: false
               });
               editModal.show();

               //Add Submit Action
               $("#frmChangePlayer").on('submit', function (e) {
                  e.preventDefault();
                  if($("#playerOut").val() == 'Spieler wählen' || $("#playerIn").val() == 'Spieler wählen' ) {
                     if($("#playerOut").val() == 'Spieler wählen') {
                        $("#playerOut").addClass('form-error');
                     }else{
                        $("#playerOut").removeClass('form-error');
                     }
                     if($("#playerIn").val() == 'Spieler wählen') {
                        $("#playerIn").addClass('form-error');
                     }else{
                        $("#playerOut").removeClass('form-error');
                     }
                     return false;
                  }
                  $.post($(this).attr('action'), $(this).serialize(), function (data) {
                     //Update Inset View
                     var objReturn = jQuery.parseJSON(data);
                     // $('input[data-player-einsatz="' + objReturn.in + '"]').prop("checked", true);
                     // $('input[data-player-einsatz="' + objReturn.out + '"]').prop("checked", false);
                  });
                  editModal.hide();
                  return false;
               });
            });
         });

      });

      var watch = $("time.stopwatch").memoStopwatch({
         autostart: false,
         startTime: $("div[data-set-start]").data('set-start')
      });


      $("button.btn-start-game").on('click', function (e) {
         e.preventDefault();
         if(!_checkOnlineStatus()) {
            return false;
         }
         if ($(this).hasClass('btn-success')) {

            //Check if Settings Complete
            if ($('input[name="pass"]').length) {
               if ($('input[name="pass"]:checked').length == 0) {
                  _createAlert('Bitte wählen Sie die anpspielende Mannschaft!');
                  return false;
               }
               $('input[name="pass"]').prop('disabled', true);
            }
            //Check gameSet closed?
            checkSetStatus();
            //change to Stop Function
            $(this).removeClass('btn-success').addClass('btn-sfbred').text('Pause');
            $("time.stopwatch").data('memoStopwatch').start();

            if ($("div.cover--gameset").length) {
               $("div.cover--gameset").removeClass('cover--show-nofade').removeClass('show');
            }

            //Start Game Set
            $.post('/ResultServices/setGameSetStatus/', {
               setId: $(".game-history").data('set'),
               gameId: $(".game-history").data('game'),
               status: 'start'
            });

         } else {
            $(this).removeClass('btn-sfbred').addClass('btn-success').text('Start');
            $("time.stopwatch").data('memoStopwatch').stop();
            $("div.cover--gameset").addClass('show');
         }
      });


      //Set Points
      var btnPointKlick = false;
      $("#btnPointA,#btnPointB").on('click', function (e) {
         if(!_checkOnlineStatus()) {
            return false;
         }
         if(btnPointKlick === false) {
            btnPointKlick = true;
            e.preventDefault();
            var teamId = $(this).data('team');
            var gID = $("input#gameViewers").data('gameid');

            if (teamId == '') {
               return false;
            }
            var pTeamA = $(".big-result").data('team-a');
            var pTeamB = $(".big-result").data('team-b');

            var blnSetStatus = false;
            if (pTeamA > 10 && pTeamB > 10) {
               //check Set Status if gamepoints > 10
               blnSetStatus = checkSetStatus();
            }
            if (!blnSetStatus) {
               $.post('/ResultServices/setPoint/' + gID, {'gameId': gID, 'teamId': teamId}, function (data) {
                  var objReturn = jQuery.parseJSON(data);
                  if (objReturn.score) {
                     $("div.big-result").text(objReturn.score);
                     reloadLiveScore();
                     checkSetStatus();
                     btnPointKlick = false;
                  }
               });
            }
         }
      });

      //Set TimeOut
      $("#btnTimeOutA,#btnTimeOutB").on('click', function (e) {
         e.preventDefault();
         if(!_checkOnlineStatus()) {
            return false;
         }
         var teamId = $(this).data('team');
         var gID = $("input#gameViewers").data('gameid');
         var btn = $(this);
         if (teamId == '') {
            return false;
         }
         $.post('/ResultServices/setTimeOut', {'gameId': gID, 'teamId': teamId}, function (data) {
            var objReturn = jQuery.parseJSON(data);
            // btn.addClass('btn-outline-gray').prop('disabled', true);
            // $(btn).next().addClass('text-gray');
            $(".badge",btn).toggleClass('show');
            $(btn).toggleClass('bg-sfbred');
            reloadLiveScore();
         });
      });

      /**
       * Close Game Set and reload new Set
       */
      $("#btnCloseSet").on('click',function(e){
         e.preventDefault();
         $.post('/ResultServices/setGameSetStatus/',{setId:$(".game-history").data('set'),gameId:$("#game").data('game'),status:'end'},function(data){
            //open new Game Set
            $("div.setFinishedMessage").slideUp();
            $("div.cover--gameset").fadeOut(function () {
               $(this).css('top', '');
               loadGameSet(data.currentSet);
            });
            $("#game-history>div.game-history").data('set-start', 0);

            if(data.blnFinished == 1) {
               //Finish entry and go to Controll page
               $("#control-tab").trigger('click');
            }
         });
      });
      $("#btnCancleCloseSet").on('click',function(e)
          {
             e.preventDefault();

             if(1)
             {
                $("div.setFinishedMessage").slideUp();
                $("div.cover--gameset").fadeOut(function(){
                   $(this).css('top','');
                });
             }
             return false;
          }
      );
   }

   //Load LiveScore
   function reloadLiveScore() {
      $.post('/ResultServices/getLiveScore/' + $(".game-history").data('game'), function (data) {
         if (data) {
            $("div.live-score-wrapper").html(data);

            $('a[data-point="delete"]').off().on('click',function(e){
               e.preventDefault();
               if(!_checkOnlineStatus()) {
                  return false;
               }
                document.getElementById("frmDeletePoint").action = $(this).attr('href');
                  deletePointModal = new Modal(document.getElementById('confirmDeleteModal'), {
                     keyboard: false
                  });
                  deletePointModal.show();
               return false;
            });

            $('a[data-point="change"]').off().on('click',function(e){
               e.preventDefault();
               if(!_checkOnlineStatus()) {
                  return false;
               }
               $.post($(this).attr('href'), function (data) {
                  var objReturn = jQuery.parseJSON(data);
                  if (objReturn.score) {
                     $("div.big-result").text(objReturn.score);
                     reloadLiveScore();
                     checkSetStatus();
                  }
               });
               return false;
            });
         }
      });
   }

   //Check TimeOutButton State
   function checkTimeOutButtonState(){
      $.post('/ResultServices/getTimeOutState/' + $("input#gameViewers").data('gameid'), function(data) {
         var objReturn = jQuery.parseJSON(data);
         /*if(objReturn.a.id){

            $("button#btnTimeOutA").addClass('btn-outline-gray').prop('disabled',true);
            $("button#btnTimeOutA").next().addClass('text-gray');
            $("button#btnTimeOutB").removeClass('btn-outline-gray').prop('disabled',false);
            $("button#btnTimeOutB").next().removeClass('text-gray');
         }else{
            $("button#btnTimeOutA").removeClass('btn-outline-gray').prop('disabled',false);
            $("button#btnTimeOutA").next().removeClass('text-gray');
            $("button#btnTimeOutB").addClass('btn-outline-gray').prop('disabled',true);
            $("button#btnTimeOutB").next().addClass('text-gray');
         }*/
      });
   }

   //Create an Alert Box
   function _createAlert(message,color){
      if(message == undefined){message = '';}
      if(color == undefined){color = 'alert-sfbred';}

      let el = $('<div id="alert" class="alert '+color+' alert-dismissible text-center fade show" role="alert"><div class="alert-body">' +
          message +
          '</div><button type="button" class="close btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
          '</div>');
      alert = new Alert($(el));
      if($("#alert").length){$("#alert").remove();}
      $(".mainlogo-wrapper").after(el);
   }




   /**
    * Prüft den Status eines Satzes
    * true = beendet | false = läuft
    */
   function checkSetStatus(){
      $.post('/ResultServices/getSetStatus',{setId:$(".game-history").data('set'),gameId:$("#game").data('game')},function(blnSetFinished){
         if(blnSetFinished == true){
            $("div.setFinishedMessage").slideDown();
            $("div.cover--gameset").css('top','440px').fadeIn();
         }
         return blnSetFinished;
      });
   }



   /**
    * Load Game Set Sheet
    * @param prmSetId
    */
   function loadGameSet(prmSetId=null)
   {
      if(prmSetId == null)
      {
         //get Current Set
         $.post('/ResultServices/getCurrentSetByGameId/'+$("#game").data('game'),function(setId){
            if(setId) {
               prmSetId = setId; //Current Set if found
            }else{
               prmSetId = 1; //default Set
            }
            loadGameSet(prmSetId);
         });
         return false;
      }

      $.post('/ResultServices/getGameSetById/'+prmSetId,{setId:prmSetId,gameId:$("#game").data('game')},function(e){
         if(e) {
            $("div#game").html(e);
            _setButtonActions();
            reloadLiveScore();
         }
      });
   }

   /**
    * 3. Kontrolle Abschluss
    * @type {Element}
    */

   var tabEl = document.querySelector('button#control-tab');
   tabEl.addEventListener('shown.bs.tab', function (e) {
      //reload Live Stat Data
      $.post('/ResultServices/getGame',{gameId: $("input#gameViewers").data('gameid')}, function(data) {
         var totalSets = parseInt(data.sets_team_a) + parseInt(data.sets_team_b);
         console.log(totalSets);
         $(".g_1_1 input").val(data.g_1_1+":"+data.g_1_2);
         $(".g_2_1 input").val(data.g_2_1+":"+data.g_2_2);
         $(".g_3_1 input").val(data.g_3_1+":"+data.g_3_2);
         if(totalSets >= 4) {
            $(".g_4_1 input").val(data.g_4_1 + ":" + data.g_4_2);
         }else{
            $(".g_4_1").remove();
         }
         if(totalSets >= 5) {
            $(".g_5_1 input").val(data.g_5_1 + ":" + data.g_5_2);
         }else{
            $(".g_5_1").remove();
         }
         if(totalSets >= 6) {
            $(".g_6_1 input").val(data.g_6_1 + ":" + data.g_6_2);
         }else{
            $(".g_6_1").remove();
         }
         if(totalSets >= 7) {
            $(".g_7_1 input").val(data.g_7_1 + ":" + data.g_7_2);
         }else{
            $(".g_7_1").remove();
         }
         $("#totalShotsA").val(data.balls_team_a);
         $("#totalShotsB").val(data.balls_team_b);
         $("#totalSetsA").val(data.sets_team_a);
         $("#totalSetsB").val(data.sets_team_b);

         if(data.winner == 'team_a') {
            $("#winnerTitle").text('Gewinner: ' + $('div[data-team_a]').data('team_a'));
         }else if(data.winner == 'team_b') {
            $("#winnerTitle").text('Gewinner: ' + $('div[data-team_b]').data('team_b'));
         }else{
            $("#winnerTitle").text('');
         }

      });
   });

   /**
   * Close Game
   */
   $("#btnCloseGame").on('click',function(e){
      e.preventDefault();
      if(!_checkOnlineStatus()) {
         return false;
      }
      //Check Signatures
      let err = 0;
      sig.forEach(function(item,idx){
         if(item.toData().length == 0){
            err++;
         }
      });
      if(err > 0){
         _createAlert('Unterschriften fehlen. Alle Unterschriften müssen vorhanden sein.');
         return false;
      }

      $(this).attr('disabled','disabled');
      $("div.cover--control").addClass('show');

      //Create Signature Images in db
      sig.forEach(function(item,index){
         $.post('/ResultServices/createSignImages',{field:item.canvas.dataset.sign,img:item.toDataURL(),gameId:$(".game-history").data('game'),setId:$(".game-history").data('set')},function(data) {
            document.location.href = '/ResultServices/createReceipt/'+$(".game-history").data('game');
         });
      });
      return false;
   });


   //Load First Game Set
   loadGameSet();

   var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
   var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new Tooltip(tooltipTriggerEl);
   });

   var sig = [];
   $("div.signature").each(function(idx,element){
      let sigPad = new SignaturePad($("canvas",element)[0]);
      sig.push(sigPad);
      $("button.btn-clear",element).on('click',function(e){
         e.preventDefault();
         sigPad.clear();
      });

   });


   function resizeCanvas(canvas) {
      var ratio =  window.devicePixelRatio || 1;
      canvas.width = canvas.offsetWidth * ratio;
      canvas.height = canvas.offsetHeight * ratio;
      canvas.getContext("2d").scale(ratio, ratio);
   }


   /**
    * Prüft den online Status
    * @returns {boolean}
    * @private
    */
   function _checkOnlineStatus() {
      clearTimeout(netStatusTimer);

      var StatusModal = document.getElementById('modalbox-network')

      if(!navigator.onLine) {
         if(networkStatus == undefined) {
            var networkStatus = new Modal(StatusModal, {
               keyboard: false
            });
         }
         if(!$(StatusModal).hasClass('show')) {
            networkStatus.show();
         }

         if(!$(".net-status").hasClass('net-status--error')) {
            $(".net-status").addClass('net-status--error');
         }
         netStatusTimer = setTimeout(_checkOnlineStatus,30000);
         return false;
      } else {
         if($(".net-status").hasClass('net-status--error')) {
            $(".net-status").removeClass('net-status--error');
         }

         if($(StatusModal).hasClass('show')) {
            $(StatusModal).hide();
         }

      }
      netStatusTimer = setTimeout(_checkOnlineStatus,30000);
      return true;
   }

   _checkOnlineStatus();

});



