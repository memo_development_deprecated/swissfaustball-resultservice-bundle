<?php declare(strict_types=1);

namespace Memo\ResultServiceBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


class ResultServiceExtension extends Extension
{
    public function load(array $mergedConfig, ContainerBuilder $container)
    {
        // Pfad zu den Konfigurationsdateien des Bandles erstellen.
        $path = __DIR__.'/../Resources/config';

        // Erstellen des Loaders für das aktuelle Verzeichnis
        $loader = new YamlFileLoader($container, new FileLocator($path));
        $loader->load('config.yml');
        $loader->load('parameters.yml');
        $loader->load('services.yml');
    }
}