<?php

/**
 * Contao Open Source CMS
 *
 * @package   ResultServiceBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\ResultServiceBundle\Module;
use Contao\BackendUser;
use Memo\ResultServiceBundle\Model\ResultserviceLogModel;
use Symfony\Component\HttpFoundation\Session\Session;
use Memo\ModClubBundle\Model\ClubModel;
use Memo\ModSwissfaustballBundle\Model\SfGamesModel;
use Memo\ModSwissfaustballBundle\Model\SfRefereeModel;
use Memo\ResultServiceBundle\Model\ResultserviceAttributesModel;
use Memo\ModClubBundle\Model\TeamModel;
use Memo\ModClubBundle\Model\PersonModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;







class ModuleResultServiceReader extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'memo_result_service_reader';

    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=memoarchive"><strong>hier verwalteten Inhalte</strong></a>.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }


    protected function compile()
    {

        $session = \Session::getInstance();

        //Get Attributes
        $ResultserviceAttributesModel = new ResultserviceAttributesModel();
        $ResultserviceLogModel = new ResultserviceLogModel();

        $aAttributes = $ResultserviceAttributesModel->findByType();

        //Get Game Data
        $prmGameId = \Input::get('gid');
        $GLOBALS['TL_CSS'][] = 'bundles/resultservice/css/result_service.css|static';

        if(empty($prmGameId))
        {
            throw new NotFoundHttpException('Spiel wurde nicht gefunden.');
            return false;
        }

        $oGame = SfGamesModel::findOneById($prmGameId);


        //Get Game Referee
        if(isset($oGame->f_referee)) {$oGame->f_referee = SfRefereeModel::findByPk($oGame->f_referee);}
        if(isset($oGame->f_writer)) {$oGame->f_writer = SfRefereeModel::findByPk($oGame->f_writer);}
        if(isset($oGame->f_linesman)) {$oGame->f_linesman = SfRefereeModel::findByPk($oGame->f_linesman);}
        if(isset($oGame->f_linesman_2)) {$oGame->f_linesman_2 = SfRefereeModel::findByPk($oGame->f_linesman_2);}

        $oGameLog = $ResultserviceLogModel->findBy(['id_game=?'],[$prmGameId],['order'=>'tstamp']);

        $aTeams['a'] = $this->_getClubInfoByTeamId($oGame->team_a);
        $aTeams['b'] = $this->_getClubInfoByTeamId($oGame->team_b);

        //Update Data
        $aPlayerChange = [];
        $aDuration = [];
        $iTotalTime = 0;
        $aStartPlayer = [];

        if(null !== $oGameLog) {
            foreach ($oGameLog as $key => $row) {
                if ($row->skey == 'gameset') {
                    if ($row->svalue === 'start') {
                        $start = $row->tstamp;

                    } elseif ($row->svalue === 'end') {
                        $duration = $row->tstamp - $start;
                        $iTotalTime += $duration;

                        $hours = floor($duration / 3600);
                        $mins = floor($duration / 60 % 60);
                        $secs = floor($duration % 60);
                        $aDuration[$row->playset] = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                    }
                }

                if ($row->skey == 'playerchange') {
                    $aChange = unserialize($row->svalue);
                    if ($oGame->team_a == $row->id_team) {
                        $aPlayer = $aTeams['a']->player;
                    } else {
                        $aPlayer = $aTeams['b']->player;
                    }
                    for ($i = 0; $i < count($aPlayer); $i++) {
                        if ($aPlayer[$i]->id == $aChange['out']) {
                            $aPlayerChange[$row->id]['out'] = $aPlayer[$i];
                        }
                        if ($aPlayer[$i]->id == $aChange['in']) {
                            $aPlayerChange[$row->id]['in'] = $aPlayer[$i];
                        }
                    }
                }
                if ($row->skey == 'warning') {
                    $row->aPlayer = unserialize($row->player);
                }
            }
        }else{
            $oGameLog = $ResultserviceLogModel;
        }

        $hours = floor($iTotalTime / 3600);
        $mins  = floor($iTotalTime / 60 % 60);
        $secs  = floor($iTotalTime % 60);

        $aStartPlayer = $ResultserviceLogModel->findStartingPlayersByGameId($prmGameId);

        $aDuration['total'] = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
        $oGame->setCount = $ResultserviceLogModel->_getCurrentPlaySetByGameId($prmGameId,true);

        //Get Attributes
        $sAttId = unserialize($oGame->attributes);
        if(!empty($sAttId)) {
            $oGame->att = $ResultserviceAttributesModel->findByType(null, $sAttId);
        }else{
            $oGame->att = null;
        }

        $this->Template->attributes   = $aAttributes;
        $this->Template->aTeams       = $aTeams;
        $this->Template->oGame        = $oGame;
        $this->Template->aStartPlayer = $aStartPlayer;
        $this->Template->gameLog      = $oGameLog;
        $this->Template->aDuration    = $aDuration;
        $this->Template->aPlayerChange= $aPlayerChange;
        $this->Template->strTemplate  = $this->strTemplate;

        $GLOBALS['TL_CSS'][] = 'bundles/resultservice/css/result_service_detail.css';
    }


    /**
     * Club + Team Info by Team ID
     * @param null $prmId
     * @return array|
     */
    static function _getClubInfoByTeamId($prmId = NULL)
    {
        if (empty($prmId)) {
            return [];
        }
        $oReturn = null;

        if (!$oReturn = TeamModel::findBy(['tl_mod_team.published=?', 'tl_mod_team.id=?'], [1, $prmId])) {
            $oReturn = new TeamModel();
        };
        if ($oReturn->pid) {
            $oReturn->club = ClubModel::findByPk($oReturn->pid);
        } else {
            $oReturn->club = new ClubModel();
        }

        $oReturn->player = PersonModel::findBy(
            array('role=?', 'published=?', 'pid=?'),
            array('player', '1', $prmId),
            array('order' => 'number ASC'));

        $oStaff = PersonModel::findBy(
            array('role=?', 'published=?', 'pid=?'),
            array('staff', '1', $prmId),
            array('order' => 'number ASC'));

        //Reorder by function
        $aStaff = [];
        foreach ($oStaff as $key => $val) {
            if (preg_match("/trainer/is", $val->function)) {
                $aStaff['trainer'] = $val;
            } else {
                $aStaff[strtolower($val->function)] = $val;
            }
        }
        $oReturn->staff = $aStaff;
        return $oReturn;
    }

}
