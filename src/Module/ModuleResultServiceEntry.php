<?php

/**
 * Contao Open Source CMS
 *
 * @package   ResultServiceBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\ResultServiceBundle\Module;
use Contao\BackendUser;
use Memo\ResultServiceBundle\Model\ResultserviceLogModel;
use Symfony\Component\HttpFoundation\Session\Session;
use Memo\ModClubBundle\Model\ClubModel;
use Memo\ModSwissfaustballBundle\Model\SfGamesModel;
use Memo\ModSwissfaustballBundle\Model\SfRefereeModel;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Memo\ResultServiceBundle\Model\ResultserviceAttributesModel;
use Memo\ModClubBundle\Model\TeamModel;
use Memo\ModClubBundle\Model\PersonModel;
use Symfony\Component\HttpFoundation\Request;





class ModuleResultServiceEntry extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'memo_result_service_entry';
    protected $currentPlaySet = 1;

    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=memoarchive"><strong>hier verwalteten Inhalte</strong></a>.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }


    protected function compile()
    {

        $aAttributes   = [];
        $aSelectedAttr = [];

        /**
         * Auth Check only 4 BackendUser fako and Admins
         */
        $objUser = BackendUser::getInstance();
        $objUser->authenticate();


        if (TL_MODE == 'FE' AND ($objUser->username == 'fako' OR $objUser->isAdmin === true)) {

            $session= \Session::getInstance();

            //Get Attributes
            $resAttModel = new ResultserviceAttributesModel();
            $aAttributes = $resAttModel->findByType();

            //Get Game Data
            $prmGameId = \Input::get('gid');

            if(empty($prmGameId))
            {
                if(empty($session->get("rsGameId"))) {
                    $thisYear = mktime(0, 0, 0, 1, 1, date("Y", time()) - 1);
                    $oGames = SfGamesModel::findBy(['round_date >= ?'], [$thisYear], ['order by round_date']);
                    $oGame = new SfGamesModel();
                }else{
                    $prmGameId = $session->get("rsGameId");
                    $oGame = SfGamesModel::findByPk($prmGameId);
                    $oGames = new SfGamesModel();
                }
            }else{
                $session->set("rsGameId",$prmGameId);
                $oGame = SfGamesModel::findByPk($prmGameId);
                $oGames = new SfGamesModel();
            }

            //get WinSets & Winner Team
            $objCategory = SfCategoriesModel::findOneBy('season_category',$oGame->category);
            $oGame->intWinSets  = $objCategory->sets;
            $oGame->winnerTeam = ResultserviceLogModel::getWinnerTeamByGame($prmGameId);

            if($oGame->sets_team_a >= $oGame->intWinSets OR $oGame->sets_team_a >= $oGame->intWinSets)
            {
                $this->Template->allSetsPlayed = true;
            }

            //Game Closed no more entries allowed
            if($oGame->rsclosed)
            {
                $this->Template->gameClosed = true;
                return false;
            }

            $resServiceLogModel     = new ResultserviceLogModel();
            $this->currentPlaySet   = $resServiceLogModel->_getCurrentPlaySetByGameId($prmGameId);

            //Get Selected Game Attributes
            if(isset($oGame->attributes)){
                $sAttId = implode(",",unserialize($oGame->attributes));
                if(!empty($sAttId)) {
                    $aAttResult = ResultserviceAttributesModel::findBy(['id in (' . $sAttId . ')', 'published=?'], [1]);
                    if(!empty($aAttResult)) {
                        foreach ($aAttResult as $key => $val) {
                            $aSelectedAttr[$val->id] = $val->title;
                        }
                    }
                }
            }

            //Get Current Result Status
            $oCurrentResultData = $resServiceLogModel->getScoreByGameId($prmGameId);

            //Get Current LiveScore
            $aLiveScore = $resServiceLogModel->getLiveScoreByGameId($prmGameId);

            //Get Game Referee
            if(isset($oGame->f_referee)) {$oGame->f_referee = SfRefereeModel::findByPk($oGame->f_referee);}
            if(isset($oGame->f_writer)) {$oGame->f_writer = SfRefereeModel::findByPk($oGame->f_writer);}
            if(isset($oGame->f_linesman)) {$oGame->f_linesman = SfRefereeModel::findByPk($oGame->f_linesman);}
            if(isset($oGame->f_linesman_2)) {$oGame->f_linesman_2 = SfRefereeModel::findByPk($oGame->f_linesman_2);}

            $aTeams['a'] = $this->_getClubInfoByTeamId($oGame->team_a);
            $aTeams['b'] = $this->_getClubInfoByTeamId($oGame->team_b);

            $this->Template->attributes         = $aAttributes;
            $this->Template->aSelectedAttr      = $aSelectedAttr;
            $this->Template->aTeams             = $aTeams;
            $this->Template->oGame              = $oGame;
            $this->Template->oGames             = $oGames;
            $this->Template->oCurrentResult     = $oCurrentResultData;
            $this->Template->aLiveScore         = $aLiveScore;
            $this->Template->currentPlaySet     = $this->currentPlaySet;
            $this->Template->strTemplate        = $this->strTemplate;
            $this->Template->gameClosed = false;

            $GLOBALS['TL_CSS'][] = 'bundles/resultservice/css/base.css|static';
            $GLOBALS['TL_CSS'][] = 'bundles/resultservice/css/bootstrap-icons.css|static';
            $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/resultservice/script.js|static';

        }else{
            \Controller::redirect('/');
        }

    }

    /**
     * Club + Team Info by Team ID
     * @param null $prmId
     * @return array|\Contao\Model|\Contao\Model[]|\Contao\Model\Collection|TeamLiveModel|null
     */
    protected function _getClubInfoByTeamId($prmId=NULL)
    {
        if(empty($prmId))
        {
            return [];
        }
        $oReturn = null;

        if(!$oReturn = TeamModel::findBy(['tl_mod_team.published=?','tl_mod_team.id=?'],[1,$prmId])){
            $oReturn = new TeamModel();
        };
        if($oReturn->pid) {
            $oReturn->club = ClubModel::findByPk($oReturn->pid);
        }else{
            $oReturn->club = new ClubModel();
        }

        $oReturn->player  = PersonModel::findBy(
            array('role=?','published=?','pid=?'),
            array('player', '1', $prmId),
            array('order' => 'number ASC'));

        $oStaff = PersonModel::findBy(
            array('role=?','published=?','pid=?'),
            array('staff', '1', $prmId),
            array('order' => 'number ASC'));

        //Reorder by function
        $aStaff = [];
        foreach($oStaff as $key => $val) {
            if (preg_match("/trainer/is", $val->function))
            {
                $aStaff['trainer'] = $val;
            }else {
                $aStaff[strtolower($val->function)] = $val;
            }
        }
        $oReturn->staff = $aStaff;
        return $oReturn;
    }


}
?>
