<?php

namespace Memo\ResultServiceBundle\Classes;

/**
 * PDF Output Glonbal Header
 */
class resultServicePdf extends \FPDF
{
    function Header()
    {
        $sFilePath  = \System::getContainer()->getParameter('kernel.project_dir')."/files";
        $this->Image($sFilePath.'/template/img/logo.png',$this->GetPageWidth()-30,10,20);
    }
}
