<?php

namespace Memo\ResultServiceBundle\Model;


use Memo\ModSwissfaustballBundle\Model\SfGamesModel;

/**
 * Class ResultServiceLog
 *
 * Reads and writes ResultServiceLog.
 */
class ResultserviceLogModel extends \Contao\Model
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_resultservice_log';

    /**
     * @param null $prmGameId
     * @param null $prmSet default = null aktueller satz (Satz ID überschreibt current set)
     * @return array|array[]
     */
    function getLiveScoreByGameId($prmGameId = NULL,$prmSet=NULL): array
    {
        $aReturn = ['a'=>[],'b'=>[],'inset'=>[],'warning'=>[],'time'=>[],'timeout'=>[],'firstplay'=>[]];

        if(empty($prmGameId)) {return $aReturn;}

        //Get Game Infos
        $objGame     = SfGamesModel::findOneBy('id',$prmGameId);
        if(is_null($prmSet)) {
            $iCurrentSet = self::_getCurrentPlaySetByGameId($prmGameId);
        }else{
            $iCurrentSet = intval($prmSet);
        }

        $oData       = self::findBy(['id_game=?'],[$prmGameId],['order'=> 'tstamp ASC']);

        $i = 0;
        $blnStartPlayer = false;
        if($oData) {
            foreach ($oData as $key => $val) {

                if($val->skey == 'gameset') {
                    if ($val->svalue === 'start') {
                        $blnStartPlayer = true;
                    }
                }

                if($iCurrentSet == $val->playset) {
                    //Nur aktuelle Satz Daten werden ausgegeben
                    if ($val->skey != 'timeout' and $val->skey != 'playerchange' and $val->skey != 'inset' and $val->skey != 'sig_spielfuehrerA' and $val->skey != 'sig_spielfuehrerB' and $val->skey != 'sig_anschreiber' and $val->skey != 'sig_schiedsrichter' and $val->skey != 'firstplay' and $val->skey != 'gameset' and $val->skey != 'warning' and $val->skey != 'incident' and $val->skey != 'objection' and $val->skey != 'interruption' and $val->skey != 'exclusion') {
                        if ($val->id_team == $objGame->team_a) {
                            $aReturn['a'][$i] = $val;
                        } else {
                            $aReturn['b'][$i] = $val;
                        }
                        $i++;
                    }
                }

                //Add Inset Data
                if($val->skey == 'inset')
                {
                    $aPlayer = unserialize($val->player);
                    if($blnStartPlayer === false) {
                        $aReturn['startPlayer'][$aPlayer['id']] = $val->svalue; //set Current Value (order by tstamp)
                    }
                    $aReturn['inset'][$aPlayer['id']] = $val->svalue; //set Current Value (order by tstamp)
                }elseif($val->skey == 'timeout' and $val->playset == $iCurrentSet)
                {
                    if($objGame->team_a == $val->id_team){
                        $aReturn['timeout']['a'] = $val;
                    }elseif($objGame->team_b == $val->id_team){
                        $aReturn['timeout']['b'] = $val;
                    }
                }elseif($val->skey == 'firstplay'){
                    $aReturn['firstplay'] = $val;
                }elseif($val->skey == 'warning'){
                    $aPlayer = unserialize($val->player);
                    $aReturn['warning'][$aPlayer['id']] .= $val->svalue." | ";
                }elseif($val->skey == 'gameset' and $val->playset == $iCurrentSet){
                    if($val->svalue == 'start')
                    {
                        $aReturn['time']['start'] = $val->tstamp;
                    }
                    if($val->svalue == 'end')
                    {
                        $aReturn['time']['end'] = $val->tstamp;
                    }
                    if(empty($aReturn['warning'][$aPlayer['id']]))
                    {
                        $aReturn['warning'][$aPlayer['id']] = '';
                    }
                    $aReturn['warning'][$aPlayer['id']] .= $val->svalue." | ";
                }

            }
        }
        return $aReturn;
    }




    /**
     * aktueller Spielstand by Game ID
     * @param null $prmGameId
     */
    public function getScoreByGameId($prmGameId=NULL)
    {
        if(empty($prmGameId))
        {
            return false;
        }


        $aReturn = ['score' => "0:0",'team_a'=>0,'team_b'=>0,'id_game'=>$prmGameId,'tstamp'=>time()];

        //Get Game Infos
        $objGame = SfGamesModel::findOneBy('id',$prmGameId);
        $iCurrentSet = self::_getCurrentPlaySetByGameId($prmGameId);
        $oData = self::findBy(['id_game=?','skey=?','playset=?'],[$prmGameId,'point',$iCurrentSet]);


        $a = 0;
        $b = 0;
        if($oData) {
            foreach ($oData as $key => $val) {
                if($val->id_team == $objGame->team_a){
                    $a++;
                }else{
                    $b++;
                }
            }
            $aReturn['score'] = sprintf("%d:%d",intval($a),intval($b));
            $aReturn['team_a'] = intval($a);
            $aReturn['team_b'] = intval($b);
        }
        return $aReturn;
    }

    /**
     * Return the current playset by gameID
     * @param null $prmGameId
     * @param bool $blnRealData = true effektiv db entry / false
     * @return bool|int|mixed|null
     */
    public function _getCurrentPlaySetByGameId($prmGameId=NULL,$blnRealData=false)
    {
        if(empty($prmGameId)) {return false;}
        $iReturn = 1;
        $oData = self::findOneBy(['id_game=?','skey=?'],[$prmGameId,'gameset'],['order'=>'tstamp desc']);

        if(null !== $oData && $oData->svalue == 'end')
        {
            if($blnRealData == true){
                $iReturn = $oData->playset;
            }else {
                $iReturn = $oData->playset + 1;
            }
        }elseif(!empty($oData->playset)){
            $iReturn = $oData->playset;
        }

        return $iReturn;
    }

    /**
     * Prüft den Status eines Satzes | Beendet = true sonst false
     * @param null $prmGameId
     * @param int $prmSetNumber
     * @return bool
     */
    public function _getSetStatusBySetNumber($prmGameId=NULL, $prmSetNumber=1)
    {
        if(empty($prmGameId)) {return false;}
        $gA = 0;
        $gB = 0;

        $objGame    = SfGamesModel::findOneBy('id',$prmGameId);
        $currentSet = self::_getCurrentPlaySetByGameId($prmGameId);
        $oData      = self::findBy(['id_game=?','skey=?','playset=?'],[$prmGameId,'point',$currentSet],['order'=>'tstamp']);

        if($oData){
            foreach($oData as $val)
            {
                if($objGame->team_a == $val->id_team)
                {
                    $gA++;
                }elseif($objGame->team_b == $val->id_team)
                {
                    $gB++;
                }
            }
        }

        if(($gA >= 11 || $gB >= 11) && abs($gA-$gB) >= 2 || ($gA === 15 || $gB ===15))
        {
            return true;
        }
        return false;
    }

    /**
     * Gewinner Team bei Game ID, unabhängig von den gespielten Sätzen.
     * Live abfrage Gewinner zum aktuellen Zeitpunkt der Abfrage.
     * Gewinner wird anhand der gewonnenen Sätze definiert.
     * @param null $prmGameId
     */
    public static function getWinnerTeamByGame($prmGameId=NULL)
    {
        if(empty($prmGameId)) {return false;}
        $gA = 0;
        $gB = 0;
        $objGame  = SfGamesModel::findOneBy('id',$prmGameId);
        $objGameLog  = self::findBy(['id_game=?','skey=?','svalue=?'],[$prmGameId,'gameset','end']);
        $countA = 0;
        $countB = 0;

        if(!empty($objGameLog)) {
            foreach ($objGameLog as $key => $val) {
                if ($val->id_team == $objGame->team_a) {
                    $countA++;
                } else {
                    $countB++;
                }
            }
        }

        if($countA > $countB)
        {
            return 'a';
        }else{
            return 'b';
        }
        return false;
    }
    /**
     * @param int|null $prmGameId
     * @return array
     */
    public static function findStartingPlayersByGameId(int $prmGameId=NULL): array
    {
        $aStartingPlayers = [];
        if(empty($prmGameId)) {
            return $aStartingPlayers;
        }

        //Get StartingPlayers
        $res = self::findBy(['id_game=?','playset=?'],[$prmGameId,1]);

        if(null !== $res) {
            //durchlauf alle Einträge bis Satz 1 gestartet wird.
            foreach ($res as $key => $val) {
                if($val->skey == 'gameset' and $val->svalue == 'start' && $val->playset > 1) {
                    //2st Set beginns, break loop
                    break;
                }

                if($val->skey == 'inset' and $val->svalue == 'true') {
                    $player = unserialize($val->player);
                    $aStartingPlayers[$val->id_team][$player['id']] = $player;
                }elseif($val->skey == 'inset' and $val->svalue == 'false') {
                    $player = unserialize($val->player);
                    unset($aStartingPlayers[$val->id_team][$player['id']]);
                }
            }
        }

        return $aStartingPlayers;
    }

}
