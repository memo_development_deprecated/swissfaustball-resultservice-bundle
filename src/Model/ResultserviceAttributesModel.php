<?php

namespace Memo\ResultServiceBundle\Model;

/**
 * Class ResultserviceAttributes
 *
 * Reads and writes ResultserviceAttributes.
 */
class ResultserviceAttributesModel extends \Contao\Model
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_resultservice_attributes';


    /**
     * @param str $prmType Type Wetter/Terrain
     * @return array|mixed
     */
    public function findByType($prmType=NULL,$aItemIds = null){

        $aResult = [];

        if(is_null($aItemIds)) {
            $aItems = self::findBy('published', 1, ['order' => 'title asc']);
        }else{
            $aItems = self::findBy(['id in (' . implode(",",$aItemIds) . ')','published=?'], [1], ['order' => 'title asc']);
        }

        if($aItems){
            foreach($aItems as $key => $val)
            {
                $aResult[$val->type][] = $val;
            }

            if(!empty($prmType) AND isset($aResult[$prmType])){
                return $aResult[$prmType];
            }else{
                return $aResult;
            }
        }

    }

}


