<?php
/**
 * @package   ResultServiceBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\ResultServiceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ResultServiceBundle extends Bundle
{
}
