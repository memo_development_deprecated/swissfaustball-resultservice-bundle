<?php

namespace Memo\ResultServiceBundle\Controller;

use Contao\CoreBundle\Controller\AbstractController;
use Contao\BackendUser;
use Memo\ModClubBundle\Model\PersonModel;
use Memo\ModSwissfaustballBundle\Model\SfGamesModel;
use Memo\ModSwissfaustballBundle\Model\SfRefereeModel;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Symfony\Component\HttpKernel\KernelInterface;

use Symfony\Component\Debug\Debug;
use Memo\ModClubBundle\Model\ClubModel;
use Memo\ModClubBundle\Model\TeamModel;
use Memo\ResultServiceBundle\Model\ResultserviceAttributesModel;
use Memo\ResultServiceBundle\Model\ResultserviceLogModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Memo\ResultServiceBundle\Classes\resultServicePdf as PDF;
use Twig\Environment as TwigEnvironment;


class ResultServiceController extends AbstractController
{

    private $blnCacheTemplates = false;
    private $blnDebug = true;
    private $kernel;
    private $serializer;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Route("/ResultServices", name=ResultService_entry::class)
     */

    public function resultServiceStep1(Request $request): Response
    {

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        //Load Module for Product, FrontendModule
        $objTeamData['a']        = $this->_getClubInfoByTeamId(1);
        $objTeamData['b']        = $this->_getClubInfoByTeamId(4);


        $aAttributes = ResultserviceAttributesModel::findByType();

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'resultservice.entry.twig',
                ['objTeamData' => $objTeamData,
                 'aAttributes' => $aAttributes
                ]
            )
        );
    }



    /**
     * Generate Edit User Modal Window
     * @Route("/ResultServices/player/edit/{playerID}",  name=PlayerEdit::class, defaults={"playerID": "NULL"})
     */

    public function editPlayerByIdAction(Request $request,$playerID): Response
    {
        if(empty($playerID))
        {
            return false;
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        //Load Module for Product, FrontendModule
        $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
        $objData        = $objObjectModel::findOneBy('id',$playerID);

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'modal.edit-player.html.twig',
                ['objData' => $objData]
            )
        );
    }

    /**
     * Generate Player Change Modal Window
     * @Route("/ResultServices/player/change/{gameId}",  name=playerChange::class, defaults={"gameId": "NULL"})
     */

    public function playerChange(Request $request, $gameId): Response
    {

        $template = $this->getTemplate();


        if(empty($gameId))
        {
            return new Response($template->render
            (
                'modal.no_access.html.twig',
                []
            ));
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        //Get Club and Team Infos
        $teamId = $request->request->get('teamId');
        $oClubInfo = $this->_getClubInfoByTeamId($teamId);

        //Get Live Score
        $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
        $aLiveScore = $oServiceLogModel->getLiveScoreByGameId($gameId);

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'modal.change-player.html.twig',
                ['objClubData' => $oClubInfo,
                    'gameId' => $gameId,
                    'aPlayerInSet' => $aLiveScore['inset']]
            )
        );

        return new Response($template->render
        (
            'modal.no_access.html.twig',
            []
        ));
    }

    /**
     * Save Player Change
     * @Route("/ResultServices/player/change/save/", name=savePlayerChange::class, defaults={})
     */
    public function savePlayerChange(Request $request): JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $prmGameId  = $request->request->get('gameId');
        $aPlayer    = $request->request->get('player');
        $prmTeamId  = $request->request->get('teamId');

        if(empty($prmGameId) OR empty($aPlayer['in']) or empty($aPlayer['out'])){
            return new JsonResponse(false);
        }

        //Create Log entry
        //Get Team Data
        $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
        $objTeam    = $objObjectModel::findByPk($prmTeamId);

        //Get Current Result
        $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
        $oCurrentResultData = $oServiceLogModel->getScoreByGameId($prmGameId);
        $currentPlaySet = $oServiceLogModel->_getCurrentPlaySetByGameId($prmGameId);

        //Create New Log entry
        $oLogEntry = new $oServiceLogModel();



        $oLogEntry->id_game = $prmGameId;
        $oLogEntry->skey    = 'playerchange';
        $oLogEntry->id_team = $prmTeamId;
        $oLogEntry->playset = $currentPlaySet;
        $oLogEntry->svalue  = serialize($aPlayer);
        $oLogEntry->score   = $oCurrentResultData['score'];
        $oLogEntry->team    = serialize($objTeam->row());
        $oLogEntry->tstamp  = time();
        $oLogEntry->save();

        //Update inset Status
        $this->_setPlayerStatus($prmGameId,$aPlayer['in'],'true');
        $this->_setPlayerStatus($prmGameId,$aPlayer['out'],'false');


        return new JsonResponse(json_encode($aPlayer));

    }


    /**
     * get Current Set by Game ID
     * @Route("/ResultServices/getCurrentSetByGameId/{gameId}",  name=getCurrentSetByGameId::class, defaults={"gameId": "NULL"})
     */

    public function getCurrentSetByGameId(Request $request,$gameId): JsonResponse
    {
        if(is_null($gameId) or !is_numeric($gameId))
        {
            return new JsonResponse();
        }
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $resServiceLogModel = new ResultserviceLogModel();
        $currentSet = $resServiceLogModel->_getCurrentPlaySetByGameId($gameId);
        return new JsonResponse($currentSet);
    }

    /**
     * Generate Modal Objection
     * @Route("/ResultServices/objection/",  name=modalObjection::class, defaults={})
     */

    public function modalObjection(Request $request): Response
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $prmGameId = $request->request->get('gid');

        if(empty($prmGameId)){
            return new Response(false);
        }

        $oGame = SfGamesModel::findByPk($prmGameId);
        $aClubs = $request->request->get('clubs');
        $aClubs['a'] = isset($aClubs[0]) ? ClubModel::findByPk($aClubs[0]) : NULL;
        $aClubs['b'] = isset($aClubs[1]) ? ClubModel::findByPk($aClubs[1]) : NULL;

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'modal.objection.html.twig',
                ['aClubs' => $aClubs,
                    'game' => $oGame
                ]
            )
        );
    }

    /**
     * Generate Modal Warning
     * @Route("/ResultServices/warning/",  name=modalWarning::class, defaults={})
     */

    public function modalWarning(Request $request): Response
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $prmGameId = $request->request->get('gid');

        if(empty($prmGameId)){
            return new Response(false);
        }

        $oGame = SfGamesModel::findByPk($prmGameId);
        $aTeams['a'] = $this->_getClubInfoByTeamId($oGame->team_a);
        $aTeams['b'] = $this->_getClubInfoByTeamId($oGame->team_b);

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'modal.warning.html.twig',
                ['aTeams' => $aTeams,
                    'game' => $oGame
                ]
            )
        );
    }

    /**
     * save Warning Modal entry
     * @Route("/ResultServices/warning/save",  name=saveWarning::class, defaults={})
     */

    public function saveWarning(Request $request): JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $aResult    = [];
        $iPlayerId  = null;
        $gid        = $request->request->get('gid');
        $sNotice    = $request->request->get('notice');
        $playerAId  = intval($request->request->get('playerTeamA'));
        $playerBId  = intval($request->request->get('playerTeamB'));

        if($playerAId > 0) {
            $iPlayerId = $playerAId;
        }
        elseif($playerBId > 0)
        {
            $iPlayerId = $playerBId;
        }


        if(!empty($gid) AND !empty($iPlayerId)) {
            $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');

            //Get Player + Team Info
            $oPlayer = PersonModel::findByPk($iPlayerId);
            $oTeam   = TeamModel::findByPk($oPlayer->pid);

            //Get current Playset
            $iPlaySet = $oServiceLogModel->_getCurrentPlaySetByGameId($gid);

            //Get Current Result
            $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gid);

            //Create New Log entry
            $oLogEntry = new $oServiceLogModel();

            $oLogEntry->id_game = $gid;
            $oLogEntry->skey    = 'warning';
            $oLogEntry->id_team = $oPlayer->pid;
            $oLogEntry->playset = $iPlaySet;
            $oLogEntry->score   = $oCurrentResultData['score'];
            $oLogEntry->team    = serialize($oTeam->row());
            $oLogEntry->svalue  = $sNotice;
            $oLogEntry->player  = serialize($oPlayer->row());
            $oLogEntry->tstamp  = time();
            $oLogEntry->save();
            return new JsonResponse(json_encode(['playerid'=>$iPlayerId,'msg'=>$sNotice]));
        }
        return new JsonResponse(false);
    }

    /**
     * Generate Modal incident
     * @Route("/ResultServices/incident/",  name=modalIncident::class, defaults={})
     */

    public function modalIncident(Request $request): Response
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $prmGameId = $request->request->get('gid');

        if(empty($prmGameId)){
            return new Response(false);
        }

        $oGame = SfGamesModel::findByPk($prmGameId);
        $aTeams['a'] = $this->_getClubInfoByTeamId($oGame->team_a);
        $aTeams['b'] = $this->_getClubInfoByTeamId($oGame->team_b);

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'modal.incident.html.twig',
                ['aTeams' => $aTeams,
                    'game' => $oGame
                ]
            )
        );
    }

    /**
     * save Einsprache Modal entry
     * @Route("/ResultServices/incident/save",  name=saveIncident::class, defaults={})
     */

    public function saveIncident(Request $request): JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $aResult = [];
        $gid    = $request->request->get('gid');
        $sNotice = $request->request->get('notice');
        $sIncident = $request->request->get('incident');
        $iPlayerId = null;

        if($request->request->get('playerTeamA') and intval($request->request->get('playerTeamA')) > 0){
            $iPlayerId = intval($request->request->get('playerTeamA'));
        }elseif($request->request->get('playerTeamB') and intval($request->request->get('playerTeamB')) > 0) {
            $iPlayerId = intval($request->request->get('playerTeamB'));
        }

        if(!empty($gid) and !empty($sNotice)) {

            $objTeam = null;
            $oPlayer = null;

            //playerInfo
            if($iPlayerId) {
                $oPlayer = PersonModel::findByPk($iPlayerId);

                //Get Team Data
                $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
                $objTeam = $objObjectModel::findByPk($oPlayer->pid);
            }

            //Get Current Result
            $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
            $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gid);
            $currentSet = $oServiceLogModel->_getCurrentPlaySetByGameId($gid);


            //Create New Log entry
            $oLogEntry = new $oServiceLogModel();

            $oLogEntry->id_game = $gid;
            $oLogEntry->skey    = $sIncident;
            $oLogEntry->svalue  = $sNotice;
            $oLogEntry->playset = $currentSet;
            $oLogEntry->score   = $oCurrentResultData['score'];
            $oLogEntry->tstamp  = time();
            if($objTeam)
            {
                $oLogEntry->team    = serialize($objTeam->row());
            }
            if($oPlayer) {
                $oLogEntry->id_team = $oPlayer->pid;
                $oLogEntry->player  = serialize($oPlayer->row());
            }
            $oLogEntry->save();
            return new JsonResponse(true);
        }
        return new JsonResponse(false);
    }

    /**
     * save Objection Modal entry
     * @Route("/ResultServices/objection/save",  name=saveObjection::class, defaults={})
     */

    public function saveObjection(Request $request): JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $aResult = [];
        $gid    = $request->request->get('gid');
        $teamId = $request->request->get('team');
        $sObjection = $request->request->get('objection');

        if(!empty($gid) AND !empty($teamId)) {

            //Get Team Data
            $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
            $objTeam    = $objObjectModel::findByPk($teamId);

            //Get Current Result
            $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
            $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gid);
            $currentSet = $oServiceLogModel->_getCurrentPlaySetByGameId($gid);

            //Create New Log entry
            $oLogEntry = new $oServiceLogModel();

            $oLogEntry->id_game = $gid;
            $oLogEntry->skey    = 'objection';
            $oLogEntry->svalue  = $sObjection;
            $oLogEntry->id_team = $teamId;
            $oLogEntry->playset = $currentSet;
            $oLogEntry->score   = $oCurrentResultData['score'];
            $oLogEntry->team    = serialize($objTeam->row());
            $oLogEntry->tstamp  = time();
            $oLogEntry->save();
            return new JsonResponse(true);
        }
        return new JsonResponse(false);
    }

    /**
     * @Route("/ResultServices/player/create", name=createPlayerModal::class)
     * @param Request $request
     * @return Response
     */
    public function createPlayerModal(Request $request): Response
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $prmTeamId = $request->request->get('teamId');

        if(empty($prmTeamId)){
            return new Response(false);
        }

        $oTeam  = $this->_getClubInfoByTeamId($prmTeamId);

        $template = $this->getTemplate();
        return new Response(
            $template->render
            (
                'modal.createPlayer.html.twig',
                ['oTeam' => $oTeam]
            )
        );
    }

    /**
     * Generate save User Modal entry
     * @Route("/ResultServices/player/save/{playerID}",  name=savePlayer::class, defaults={"playerID": "NULL"})
     */

    public function savePlayer(Request $request,$playerID): JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $aResult = [];
        $bDateYear = $request->request->get('birthday');
        $prmTeamId = $request->request->get('teamId');

        if(is_numeric($playerID)) {
            $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
            $objPlayer = $objObjectModel::findOneBy('id',$playerID);

            //Calc new Birthdate
            if(!empty($bDateYear)){
                $sBirthDate = mktime(date('h',$objPlayer->birthdate),date('m',$objPlayer->birthdate),date('s',$objPlayer->birthdate),date('m',$objPlayer->birthdate),date('d',$objPlayer->birthdate),$bDateYear);
                $objPlayer->birthdate = $sBirthDate;
            }
            $objPlayer->name = $request->request->get('name');
            $objPlayer->firstname = $request->request->get('firstname');
            $objPlayer->number = $request->request->get('number');
            $objPlayer->diffFields  = serialize(['name','firstname','number','birthdate']);
            $objPlayer->save();
            $objPlayerArray = ['id'=>$objPlayer->id,'name'=>$objPlayer->name,'firstname'=>$objPlayer->firstname,'number'=>$objPlayer->number,'birthdate'=>$objPlayer->birthdate];

            return new JsonResponse(json_encode($objPlayerArray));
        }elseif(is_numeric($prmTeamId)){
            //Create New Player
            $objPlayerModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
            $objPlayer = new $objPlayerModel();
            $objPlayer->name        = $request->request->get('name');
            $objPlayer->firstname   = $request->request->get('firstname');
            $objPlayer->number      = $request->request->get('number');
            $objPlayer->tstamp      = time();
            $objPlayer->role        = 'player';
            $objPlayer->published   = 1;
            $objPlayer->birthdate   = mktime(0,0,0,1,1,$bDateYear);
            $objPlayer->pid         = $prmTeamId;
            $objPlayer->diffFields  = serialize(['name','firstname','number','role','birthdate']);
            $objPlayer->save();
        }
        return new JsonResponse(false);

    }

    /**
     * Generate save Player Modal entry
     * @Route("/ResultServices/viewers/change/{gameID}",  name=changeGameViewerCount::class, defaults={"gameID": "NULL"})
     */

    public function changeGameViewerCount(Request $request,$gameID): JsonResponse
    {
        if(empty($gameID) OR intval($gameID) < 1){
            return new JsonResponse(false);
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
        $objGame = $objObjectModel::findOneBy('id',$gameID);
        $objGame->viewers = intval($request->request->get('viewers'));
        $objGame->save();

        return new JsonResponse(true);
    }

    /**
     * Async save Player Status in game = 1 | not in game = 0
     * @Route("/ResultServices/player/status/",  name=setPlayerStatus::class, defaults={"gameId": "NULL"})
     */
    public function setPlayerStatus(Request $request): JsonResponse
    {
        $gameId     = $request->request->get('gameId');
        $playerId   = $request->request->get('playerId');
        $blnStatus  = $request->request->get('status');

        $blnReturn = $this->_setPlayerStatus($gameId,$playerId,$blnStatus);

        return new JsonResponse($blnReturn);
    }

    /**
     * Async setCaptain
     * @Route("/ResultServices/setCaptain/",  name=setTeamCaptain::class, defaults={"gameId": "NULL"})
     */
    public function setTeamCaptain(Request $request): JsonResponse
    {
        $gameId     = $request->request->get('gameId');
        $playerId   = $request->request->get('playerId');
        $blnStatus  = $request->request->get('status');

        return new JsonResponse($this->_setCaptain($gameId,$playerId,$blnStatus));

    }


    /**
     * @Route("/ResultServices/game/getTeamStaff",name=getTeamStaff::class)
     * @param Request $request
     * @return JsonResponse
     */
    public function getTeamStaff(Request $request): JsonResponse
    {
        $term   = trim($request->get("term"));
        $teamId = intval($request->get("tid"));

        $aSearchPhrase  = explode(" ",$term);
        $aSearchKey     = ['function !=?','published = ?','pid= ?'];
        $aSearchValues  = ['player',1,$teamId];
        $aReturn        = [];

        if(empty($teamId))
        {
            return new JsonResponse($aReturn);
        }

        for($i=0;$i<count($aSearchPhrase);$i++)
        {
            $aSearchKey[]  = "(firstname like '%%".$aSearchPhrase[$i]."%%' or name like '%%".$aSearchPhrase[$i]."%%')";
            $aSearchValues[]  = '';
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $objPersonModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
        $oPerson   = $objPersonModel::findBy($aSearchKey,$aSearchValues);

        if(!is_null($oPerson)) {
            foreach($oPerson as $key => $val) {
                $aReturn[] = $val->firstname ." ".$val->name;
            }
        }
        return new JsonResponse($aReturn);
    }

    /**
     * @Route("/ResultServices/game/getOfficials",name=getGameOfficials::class)
     * @param Request $request
     * @return JsonResponse
     */
    public function getGameOfficials(Request $request): JsonResponse
    {

        $term = trim($request->get("term"));
        $aSearchPhrase  = explode(" ",$term);
        $aSearchKey     = [];
        $aSearchValues  = [];
        $aReturn        = [];

        for($i=0;$i<count($aSearchPhrase);$i++)
        {
            $aSearchKey[]     = '(firstname like "%%'.$aSearchPhrase[$i].'%%" OR name like "%%'.$aSearchPhrase[$i].'%%" ?)';
            $aSearchValues[]  = '';
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $objRefereeModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfRefereeModel');
        $oReferee   = $objRefereeModel::findBy($aSearchKey,$aSearchValues);

        if(!is_null($oReferee)) {
            foreach($oReferee as $key => $val) {
                $aReturn[] = $val->firstname ." ".$val->name;
            }
        }
        return new JsonResponse($aReturn);
    }

    /**
     * @Route("/ResultServices/game/setOfficials",name=setGameOfficials::class)
     */
    public function setGameOfficials(Request $request): JsonResponse
    {
        $gameId     = $request->request->get('gameId');
        $sKey       = $request->request->get('type');
        $sValue     = $request->request->get('value');

        if(!empty($gameId) and !empty($sKey) and !empty($sValue))
        {
            //Init Contao Framework
            $objContaoFramework = $this->container->get('contao.framework');
            $objContaoFramework->initialize();

            $aSearchPhrase  = explode(" ",$sValue);
            $aReturn        = [];

            $objRefereeModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfRefereeModel');
            $oReferee           = $objRefereeModel::findOneBy(['firstname = ?','name = ?'],[$aSearchPhrase[0],$aSearchPhrase[1]]);

            //Create Referee
            if(is_null($oReferee))
            {
                $oReferee = new SfRefereeModel();
                $oReferee->tstamp = time();
                $oReferee->published = 1;
                if(isset($aSearchPhrase[0]))
                {
                    $oReferee->firstname = trim($aSearchPhrase[0]);
                }
                if(isset($aSearchPhrase[1]))
                {
                    $oReferee->name = trim($aSearchPhrase[1]);
                }
                $oReferee->save();
            }

            //Link to Games
            $objObjectModel = $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
            $objGame = $objObjectModel::findOneBy('id', $gameId);

            $objGame->$sKey = $oReferee->id;
            $objGame->save();
        }

        return new JsonResponse(true);
    }


    /**
     * @Route("/ResultServices/game/setTeamStaff",name=setTeamStaff::class)
     */
    public function setTeamStaff(Request $request): JsonResponse
    {
        $gameId     = intval($request->request->get('gameId'));
        $teamId     = intval($request->request->get('tid'));
        $sKey       = $request->request->get('type');
        $sValue     = $request->request->get('value');

        if(!empty($gameId) and !empty($sKey) and !empty($sValue) and !empty($teamId))
        {
            //Init Contao Framework
            $objContaoFramework = $this->container->get('contao.framework');
            $objContaoFramework->initialize();

            $aSearchPhrase  = explode(" ",$sValue);
            $aSearchKey     = [];
            $aSearchValues  = [];
            $aReturn        = [];

            for($i=0;$i<count($aSearchPhrase);$i++)
            {
                $aSearchKey[]     = '(firstname like ? OR name like "'.$aSearchPhrase[$i].'")';
                $aSearchValues[]  = $aSearchPhrase[$i];
            }

            $objPersonModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
            $objPersonModel = $objPersonModel::findOneBy($aSearchKey,$aSearchValues);

            //Create Person
            if(is_null($objPersonModel))
            {
                $objPersonModel = new PersonModel();
                $objPersonModel->tstamp = time();
                $objPersonModel->published = 1;
                $objPersonModel->pid = $teamId;
                $objPersonModel->function = $sKey;
                $objPersonModel->diffFields = serialize(['name','firstname','function']);
                if(isset($aSearchValues[0]))
                {
                    $objPersonModel->firstname = trim($aSearchValues[0]);
                }
                if(isset($aSearchValues[1]))
                {
                    $objPersonModel->name = trim($aSearchValues[1]);
                }
                //$objPersonModel->save();
            }

            //Link to Games
            $objObjectModel = $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
            $objGame = $objObjectModel::findOneBy('id', $gameId);

            if($objGame->team_a == $teamId)
            {
                if($sKey == 'Trainer') {
                    $objGame->team_a_trainer = $objPersonModel->firstname . " " . $objPersonModel->name;
                }else{
                    $objGame->team_a_supervisor = $objPersonModel->firstname . " " . $objPersonModel->name;
                }
            }elseif($objGame->team_b == $teamId)
            {
                if($sKey == 'Trainer') {
                    $objGame->team_b_trainer = $objPersonModel->firstname . " " . $objPersonModel->name;
                }else{
                    $objGame->team_b_supervisor = $objPersonModel->firstname . " " . $objPersonModel->name;
                }
            }

            $objGame->save();
        }

        return new JsonResponse(true);
    }

    /**
     * Async save Game Attributes
     * @Route("/ResultServices/attributes/change/{gameId}",  name=changeGameAttributes::class, defaults={"gameId": "NULL"})
     */

    public function changeGameAttributes(Request $request,$gameId): JsonResponse
    {
        if(empty($gameId) OR intval($gameId) < 1){
            return new JsonResponse(false);
        }

        $aAttributes = [];

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
        $objGame = $objObjectModel::findOneBy('id',$gameId);
        if($objGame->attributes) {
            $aAttributes = unserialize($objGame->attributes);
        }
        if($request->request->get('status') == 'false' and in_array($request->request->get('attrId'),$aAttributes)){
            //remove key
            unset($aAttributes[array_search($request->request->get('attrId'),$aAttributes)]);
        }else{
            array_push($aAttributes,intval($request->request->get('attrId')));
        }

        $objGame->attributes = serialize($aAttributes);
        $objGame->save();
        return new JsonResponse(true);
    }

    /**
     * Async set Game TimeOut
     * @Route("/ResultServices/setTimeOut/{gameId}",  name=setTimeOut::class, defaults={"gameId": "NULL"})
     */
    public function setTimeOut(Request $request,$gameId):JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $gameId = $request->request->get('gameId');
        $teamId = $request->request->get('teamId');

        /**
         * Auth Check only 4 BackendUser fako and Admins
         * Check if user is logged in an have rights
         */
        $objUser = BackendUser::getInstance();
        $objUser->authenticate();

        if ($objUser->username == 'fako' or $objUser->isAdmin === true) {
            //$session=\Session::getInstance();

            //GameId Check
            if (empty($gameId) or intval($gameId) < 1) {
                return new JsonResponse(false);
            }

            //TeamId Check
            if (empty($teamId) or intval($teamId) < 1) {
                return new JsonResponse(false);
            }

            //Get Game Data
            $objObjectModel = $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
            $objGame = $objObjectModel::findOneBy('id', $gameId);

            //Get Team Data
            $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
            $objTeam = $objObjectModel::findOneBy('id', $teamId);

            //Get Current Result
            $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
            $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gameId);

            //Get Current Game-Set
            $currentSet = $oServiceLogModel->_getCurrentPlaySetByGameId($gameId);

            //Create New Log entry
            $oLogEntry = $oServiceLogModel::findOneBy(['id_game=?','skey=?','id_team=?','playset=?'],[$gameId,'timeout',$teamId,$currentSet]);
            //Create new One else delete
            if(is_null($oLogEntry)) {
                $oLogEntry = new $oServiceLogModel();
            }else{
                $oLogEntry->delete();
                return new JsonResponse(json_encode($oCurrentResultData));
            }

            $oLogEntry->id_game = $gameId;
            $oLogEntry->skey    = 'timeout';
            $oLogEntry->id_team  = $teamId;
            $oLogEntry->playset = $currentSet;
            $oLogEntry->score   = $oCurrentResultData['score'];
            $oLogEntry->team    = serialize($objTeam->row());
            $oLogEntry->tstamp  = time();
            $oLogEntry->save();
            return new JsonResponse(json_encode($oCurrentResultData));
        }

        return new JsonResponse(false);
    }



    /**
     * Async save Game Point
     * @Route("/ResultServices/setPoint/{gameId}",  name=setGamePoint::class, defaults={"gameId": "NULL"})
     */
    public function setGamePoint(Request $request,$gameId):JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $gameId = $request->request->get('gameId');
        $teamId = $request->request->get('teamId');

        /**
         * Auth Check only 4 BackendUser fako and Admins
         * Check if user is logged in an have rights
         */
        $objUser = BackendUser::getInstance();
        $objUser->authenticate();

        if ($objUser->username == 'fako' OR $objUser->isAdmin === true) {
            //$session=\Session::getInstance();

            //GameId Check
            if(empty($gameId) OR intval($gameId) < 1){
                return new JsonResponse(false);
            }

            //TeamId Check
            if(empty($teamId) OR intval($teamId) < 1)
            {
                return new JsonResponse(false);
            }

            //Get Game Data
            $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
            $objGame = $objObjectModel::findOneBy('id',$gameId);

            //Get Team Data
            $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
            $objTeam = $objObjectModel::findOneBy('id',$teamId);

            //Get Current Result
            $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');

            $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gameId);
            $currentSet = $oServiceLogModel->_getCurrentPlaySetByGameId($gameId);

            //Create New Log entry
            $oLogEntry = new $oServiceLogModel();

            $oLogEntry->id_game = $gameId;
            $oLogEntry->skey    = 'point';
            $oLogEntry->id_team  = $teamId;
            $oLogEntry->playset = $currentSet;
            $oLogEntry->score   = $oCurrentResultData['score'];
            $oLogEntry->team    = serialize($objTeam->row());
            $oLogEntry->tstamp  = time();
            $oLogEntry->save();

            //Update Result
            $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gameId);

            return new JsonResponse(json_encode($oCurrentResultData));
        }

        return new JsonResponse(false);
    }



    /**
     * Async delete Game Point
     * @Route("/ResultServices/deletePoint/{itemId}",  name=deleteGamePoint::class, defaults={"itemId": "NULL"})
     */
    public function deleteGamePoint(Request $request,$itemId):JsonResponse
    {
        if (empty($itemId)) {
            return new JsonResponse(false);
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');

        /**
         * Auth Check only 4 BackendUser fako and Admins
         * Check if user is logged in an have rights
         */
        $objUser = BackendUser::getInstance();
        $objUser->authenticate();

        if ($objUser->username == 'fako' or $objUser->isAdmin === true) {
            //$session=\Session::getInstance();
            $oLogEntry = $oServiceLogModel->findByPk($itemId);
            if (empty($oLogEntry->id_game)) {
                return new JsonResponse(false);
            }
            $oLogEntry->delete();
            $aLiveScore = $oServiceLogModel->getScoreByGameId($oLogEntry->id_game);

            return new JsonResponse(json_encode($aLiveScore));
        }
        return new JsonResponse(false);
    }


    /**
     * Async change Game Point
     * @Route("/ResultServices/changePoint/{itemId}",  name=changeGamePoint::class, defaults={"itemId": "NULL"})
     */
    public function changeGamePoint(Request $request,$itemId):JsonResponse
    {
        if(empty($itemId))
        {
            return new JsonResponse(false);
        }
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        /**
         * Auth Check only 4 BackendUser fako and Admins
         * Check if user is logged in an have rights
         */
        $objUser = BackendUser::getInstance();
        $objUser->authenticate();

        if ($objUser->username == 'fako' or $objUser->isAdmin === true) {
            $oServiceLogModel = $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
            //$session=\Session::getInstance();
            $oLogEntry = $oServiceLogModel->findByPk($itemId);
            if(empty($oLogEntry->id_game)){
                return new JsonResponse(false);
            }
            $oGame = SfGamesModel::findByPk($oLogEntry->id_game);

            //Change Team
            $oLogEntry->id_team = ($oGame->team_a == $oLogEntry->id_team)? $oGame->team_b : $oGame->team_a;
            $objTeamModel	 = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
            $oLogEntry->team = $objTeamModel::findByPk($oLogEntry->id_team);
            $oLogEntry->save();

            $aLiveScore = $oServiceLogModel->getScoreByGameId($oLogEntry->id_game);

            return new JsonResponse(json_encode($aLiveScore));

        }
        return new JsonResponse(false);
    }

    /**
     * Async setFirstBall
     * @Route("/ResultServices/setFirstBall/{gameId}",  name=setFirstBall::class, defaults={"gameId": "NULL"})
     */
    public function setFirstBall(Request $request,$gameId): JsonResponse
    {
        if(empty($gameId)){return new JsonResponse(false);}

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $oServiceLogModel = $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
        $prmTeamId = $request->request->get('teamId');

        $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
        $objTeam    = $objObjectModel::findByPk($prmTeamId);

        //Get Current Result
        $oCurrentResultData = $oServiceLogModel->getScoreByGameId($gameId);
        $currentPlaySet     = $oServiceLogModel->_getCurrentPlaySetByGameId($gameId);
        $oFirstPlay         = $oServiceLogModel->countBy(['id_game=?','skey=?','playset=?'],[$gameId,'firstplay',$currentPlaySet]);

        //Anspiel nur im Satz 1 / 7
        if(($currentPlaySet == 1 or $currentPlaySet == 7)) {
           //Create New Log entry
            $oLogEntry = $oServiceLogModel::findBy(['id_game=?','playset=?','skey=?'],[$gameId,$currentPlaySet,'firstplay']);

            if(is_null($oLogEntry)) {
                $oLogEntry = new $oServiceLogModel();
            }

            $oLogEntry->id_game = $gameId;
            $oLogEntry->skey = 'firstplay';
            $oLogEntry->id_team = $prmTeamId;
            $oLogEntry->playset = $currentPlaySet;
            $oLogEntry->score = $oCurrentResultData['score'];
            $oLogEntry->team = serialize($objTeam->row());
            $oLogEntry->tstamp = time();
            $oLogEntry->save();
            return new JsonResponse(true);
        }

        return new JsonResponse(false);
    }

    /**
     * Async getGameSet by Set ID and Game ID
     * @Route("/ResultServices/getGameSetById/{setId}",  name=getGameSetById::class, defaults={"setId": "1"})
     **/
    public function getGameSetById(Request $request,$setId): Response
    {
        //Init Contao Framework
        $this->container->get('contao.framework')->initialize();

        $aReturn    = [];
        $prmGameId  = $request->get('gameId');
        if(empty($prmGameId))
        {
            return new Response();
        }

        $oGame = SfGamesModel::findByPk($prmGameId);

        //All Sets played, no more games
        $objCategory = SfCategoriesModel::findOneBy('season_category',$oGame->category);
        $intWinSets  = $objCategory->sets;
        $allSetsPlayed = false;
        if($oGame->sets_team_a >= $intWinSets OR $oGame->sets_team_a >= $intWinSets)
        {
            $allSetsPlayed = true;
        }

        //Get Current Result Status
        $resServiceLogModel = new ResultserviceLogModel();
        $oCurrentResultData = $resServiceLogModel->getScoreByGameId($prmGameId);

        //Get Current LiveScore
        $aLiveScore = $resServiceLogModel->getLiveScoreByGameId($prmGameId);


        //Get Game Referee
        if(isset($oGame->f_referee)) {$oGame->f_referee = SfRefereeModel::findByPk($oGame->f_referee);}
        if(isset($oGame->f_writer)) {$oGame->f_writer = SfRefereeModel::findByPk($oGame->f_writer);}
        if(isset($oGame->f_linesman)) {$oGame->f_linesman = SfRefereeModel::findByPk($oGame->f_linesman);}
        if(isset($oGame->f_linesman_2)) {$oGame->f_linesman_2 = SfRefereeModel::findByPk($oGame->f_linesman_2);}

        $aTeams['a'] = $this->_getClubInfoByTeamId($oGame->team_a);
        $aTeams['b'] = $this->_getClubInfoByTeamId($oGame->team_b);

        $template = $this->getTemplate();

        return new Response(
            $template->render
            (
                '_include/gameSet.inc.twig',
                ['aLiveScore' => $aLiveScore,
                    'currentPlaySet' => $setId,
                    'aTeams' => $aTeams,
                    'oGame' => $oGame,
                    'allSetsPlayed' => $allSetsPlayed,
                    'oCurrentResult' => $oCurrentResultData

                ]
            )
        );
    }

    /**
     * Async getLiveScore Table
     * @Route("/ResultServices/getTimeOutState/{gameId}",  name=getTimeOutState::class, defaults={"gameId": "NULL"})
     */
    public function getTimeOutState(Request $request,$gameId): JsonResponse
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $oServiceLogModel = $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');

        $aReturn = [];

        if (!empty($gameId)) {
            $aLiveScore = $oServiceLogModel->getLiveScoreByGameId($gameId);

            $aReturn['a'] = empty($aLiveScore['timeout']['a'])? [] : $aLiveScore['timeout']['a']->row();
            $aReturn['b'] = empty($aLiveScore['timeout']['b'])? [] : $aLiveScore['timeout']['b']->row();
        }
        return new JsonResponse(json_encode($aReturn,JSON_INVALID_UTF8_SUBSTITUTE));
    }


    /**
     * Async getLiveScore Table
     * @Route("/ResultServices/getLiveScore/{gameId}",  name=getLiveScore::class, defaults={"gameId": "NULL"})
     */
    public function getLiveScore(Request $request,$gameId): Response
    {
        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $resServiceLogModel = new ResultserviceLogModel();
        $aData = [];

        if(!empty($gameId))
        {
            $aLiveScore = $resServiceLogModel->getLiveScoreByGameId($gameId);
            $template = $this->getTemplate();
            return new Response(
                $template->render
                (
                    'liveScoreTable.twig',
                    ['aLiveScore' => $aLiveScore

                    ]
                )
            );
        }


        return new Response('');
    }


    /**
     * Generate Twig Template Src
     * @param null $prmTemplateName
     * @return TwigEnvironment
     */
    private function getTemplate($prmTemplateName = NULL)
    {
        $strBundleTemplateDir = $this->kernel->locateResource('@ResultServiceBundle/Resources/contao/templates');
        $strCacheDir= $this->blnCacheTemplates === true ? $this->kernel->getProjectDir()."/var/cache" : false;
        $loader     = new \Twig\Loader\FilesystemLoader($strBundleTemplateDir);
        $twig       = new \Twig\Environment($loader, [
            'cache' => $strCacheDir,
        ]);

        if(empty($prmTemplateName))
        {
            return $twig;
        }else{
            return $twig->load($prmTemplateName);;
        }
    }


    /**
     * @param $prmId
     * @return array|mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function _getClubInfoByTeamId($prmId=NULL)
    {
        if(empty($prmId))
        {
            return [];
        }
        $oReturn = false;

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $objTeamModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
        $objPersonModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
        $objClubModel	= $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\ClubModel');

        if(!$oReturn        = $objTeamModel::findBy(['tl_mod_team.published=?','tl_mod_team.id=?'],[1,$prmId])){
            $oReturn = new $objTeamModel;
        }

        if(null !== $oReturn->pid) {
            $oReturn->club  = $objClubModel::findByPk($oReturn->pid);
        } else {
            $oReturn->club = new ClubModel();
        }

        //Get Logo
        if(null !== $oReturn->club->logoImage) {
            $oReturn->club->logoImg = \FilesModel::findByUuid($oReturn->club->logoImage);
        }else{
            $logoImg = new \FilesModel();
            $oReturn->club->logoImg = $logoImg;
        }

        $oReturn->player = $objPersonModel::findBy(
            array('role=?','published=?','pid=?'),
            array('player', '1', $prmId),
            array('order' => 'number ASC'));

        $oReturn->staff = $objPersonModel::findBy(
            array('role=?','published=?','pid=?'),
            array('staff', '1', $prmId),
            array('order' => 'number ASC'));
        return $oReturn;
    }

    /**
     * set Player Status on log - table
     * @param null $gameId
     * @param null $playerId
     * @param string $blnStatus
     * @return bool
     */
    private function _setPlayerStatus($gameId=NULL, $playerId=NULL,$blnStatus='false'){
        if(empty($gameId) OR intval($gameId) < 1 OR empty($playerId)){
            return false;
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $objObjectModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
        $objGame = $objObjectModel::findOneBy('id',$gameId);

        //Get Player Data
        $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
        $objPlayer = $objObjectModel::findOneBy('id', $playerId);

        //Get Team Data
        $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
        $objTeam = $objObjectModel::findOneBy('id', $objPlayer->pid);

        //Get Current Result
        $resServiceLogModel = new ResultserviceLogModel();
        $oCurrentResultData = $resServiceLogModel->getScoreByGameId($gameId);

        //Create New Log entry
        $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
        $iCurrentPlaySet    = $oServiceLogModel->_getCurrentPlaySetByGameId($gameId);
        $oLogEntry = new $oServiceLogModel();

        $oLogEntry->id_game = $gameId;
        $oLogEntry->id_team = $objTeam->id;
        $oLogEntry->skey    = 'inset';
        $oLogEntry->svalue  = $blnStatus;
        $oLogEntry->playset = $iCurrentPlaySet;
        $oLogEntry->score   = $oCurrentResultData['score'];
        $oLogEntry->team    = serialize($objTeam->row());
        $oLogEntry->player  = serialize($objPlayer->row());
        $oLogEntry->tstamp  = time();
        $oLogEntry->save();
        return true;
    }


    /**
     * set Team Captain
     * @param null $gameId
     * @param null $playerId
     * @param string $blnStatus
     * @return bool
     */
    private function _setCaptain($gameId=NULL, $playerId=NULL,$blnStatus='false'){
        if(empty($gameId) OR intval($gameId) < 1 OR empty($playerId)){
            return false;
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        //Get Player Data
        $objObjectPlayerModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\PersonModel');
        $objPlayer = $objObjectPlayerModel::findOneBy('id', $playerId);

        if(is_null($objPlayer))
        {
            return false;
        }

        //Get Team Data
        $objObjectModel = $objContaoFramework->createInstance('\Memo\ModClubBundle\Model\TeamModel');
        $objTeam = $objObjectModel::findOneBy('id', $objPlayer->pid);

        //Remove old Captain
        $oOldCaptain = $objObjectPlayerModel::findOneBy(['isCaptain=?','pid=?'], [1,$objPlayer->pid]);
        if(!is_null($oOldCaptain)) {
            $oOldCaptain->isCaptain = 0;
            $oOldCaptain->save();
        }

        //Set new Captain
        $objPlayer->isCaptain = 1;
        $objPlayer->diffFields  = serialize(['isCaptain']);
        $objPlayer->save();

        $objPlayerArray = ['id'=>$objPlayer->id,'name'=>$objPlayer->name,'firstname'=>$objPlayer->firstname,'number'=>$objPlayer->number,'birthdate'=>$objPlayer->birthdate,'isCaptain'=>1];

        return $objPlayerArray;
    }


    /**
     * Async getLiveScore Table
     * @Route("/ResultServices/getSetStatus",  name=getSetStatus::class)
     */
    public function _getSetStatus(Request $request): JsonResponse
    {
        $prmGameId = $request->request->get('gameId');
        $prmSetId  = $request->request->get('setId');
        $bReturn   = false;

        if(!empty($prmSetId) AND !empty($prmGameId)){
            //Init Contao Framework
            $objContaoFramework = $this->container->get('contao.framework');
            $objContaoFramework->initialize();
            $resServiceLogModel = new ResultserviceLogModel();
            $bReturn = $resServiceLogModel->_getSetStatusBySetNumber($prmGameId,$prmSetId);
        }
        return new JsonResponse($bReturn);
    }


    /**
     * Async setGameSetStatus
     * @Route("/ResultServices/setGameSetStatus/",  name=setGameSetStatus::class)
     */
    public function setGameSetStatus(Request $request): JsonResponse
    {
        $bReturn = false;
        $prmGameId = $request->request->get('gameId');
        $prmSetId  = $request->request->get('setId');
        $prmStatus = $request->request->get('status');

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $objGameModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfGamesModel');
        $objCatModel	= $objContaoFramework->createInstance('\Memo\ModSwissfaustballBundle\Model\SfCategoriesModel');
        $objGame = $objGameModel::findOneBy('id',$prmGameId);

        //Get Winset Count | Evaluate if game is finished / default 3 win sets
        $intWinSets  = 3;
        $blnCloseGame= 0;
        $objCategory = $objCatModel::findOneBy('season_category',$objGame->category);
        $intWinSets  = $objCategory->sets;

        //Get Current Result
        $resServiceLogModel = new ResultserviceLogModel();
        $oCurrentResultData = $resServiceLogModel->getScoreByGameId($prmGameId);
        $oCurrentStatus     = $resServiceLogModel->findOneBy(['id_game=?','playset=?','skey=?'],[$prmGameId,$prmSetId,'gameset'],['order'=>'tstamp desc']);

        if(empty($oCurrentStatus) OR $oCurrentStatus->svalue != $prmStatus) {
            //Create New Log entry
            $oServiceLogModel = $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
            $oLogEntry = new $oServiceLogModel();

            $oLogEntry->id_game = $prmGameId;
            $oLogEntry->skey = 'gameset';
            $oLogEntry->svalue = $prmStatus;
            $oLogEntry->playset = $prmSetId;
            $oLogEntry->score = $oCurrentResultData['score'];
            $oLogEntry->tstamp = time();

            //Set Winner Team
            if($oCurrentResultData['team_a'] > $oCurrentResultData['team_b'])
            {
                $oLogEntry->id_team = $objGame->team_a;
            }
            elseif($oCurrentResultData['team_b'] > $oCurrentResultData['team_a'])
            {
                $oLogEntry->id_team = $objGame->team_b;
            }

            $oLogEntry->save();
            $bReturn = true;
            if($prmStatus == 'end') {
                //Save Game Status
                $this->_updateGameStat($prmGameId,$prmStatus,$prmSetId);
                $objGame = $objGameModel::findOneBy('id',$prmGameId);
                if($objGame->sets_team_a >= $intWinSets or $objGame->sets_team_b >= $intWinSets) {
                    $blnCloseGame = 1;
                }else{
                    $prmSetId++;
                }
            }
        }
        return new JsonResponse(['status'=>$bReturn,'currentSet'=>$prmSetId,'blnFinished'=>$blnCloseGame]);
    }

    /**
     * Update Game Statistik tl_sf_games
     * @param null $prmGameId
     * @return boolen
     */
    private function _updateGameStat($prmGameId=NULL,$prmSetStatus = NULL, $prmSetId=NULL)
    {
        if(empty($prmGameId) or intval($prmGameId) == 0)
        {
            return false;
        }

        $resServiceLogModel = new ResultserviceLogModel();
        $oGame      = SfGamesModel::findByPk($prmGameId);
        $iPlaySet   = $resServiceLogModel->_getCurrentPlaySetByGameId($prmGameId);
        $oGameLog   = $resServiceLogModel->findBy(['id_game=?'],[$prmGameId]);

        $balls_team_a = 0;  //Zähler für #Gutbälle
        $balls_team_b = 0;  //Zähler für #Gutbälle
        $sets_team_a  = 0;  //Zähler für #gewonnener Sätze
        $sets_team_b  = 0;  //Zähler für #gewonnener Sätze
        $set_count_a  = 0;  //Zähler für Satz End Resultat
        $set_count_b  = 0;  //Zähler für Satz End Resultat
        $g_1_1 = 0;

        //Recalc Game Statistic
        if($oGameLog)
        {
            foreach($oGameLog as $key => $val)
            {
                if($val->id_team == $oGame->team_a){
                    if($val->skey == 'point'){
                        $balls_team_a++;
                    }
                    elseif($val->skey == 'gameset')
                    {
                        if($val->svalue == 'end'){
                            $sets_team_a++;
                        }
                    }

                    if(!is_null($prmSetId) and $val->playset == $prmSetId and $val->skey == 'point')
                    {
                        $set_count_a = $set_count_a+1;
                    }

                }else{
                    if($val->skey == 'point') {
                        $balls_team_b++;
                    }
                    elseif($val->skey == 'gameset')
                    {
                        if($val->svalue == 'end'){
                            $sets_team_b++;
                        }
                    }
                    if(!is_null($prmSetId) and $val->playset == $prmSetId and $val->skey == 'point')
                    {
                        $set_count_b = $set_count_b+1;
                    }
                }
            }
        }

        //Update GameSet
        $oGame->sets_team_a = $sets_team_a;
        $oGame->sets_team_b = $sets_team_b;
        $oGame->balls_team_a = $balls_team_a;
        $oGame->balls_team_b = $balls_team_b;

        if(!is_null($prmSetId) and !is_null($prmSetStatus) and $prmSetStatus == 'end')
        {
            $fieldA = sprintf("g_%d_%d",$prmSetId,1);
            $fieldB = sprintf("g_%d_%d",$prmSetId,2);
            $oGame->$fieldA = $set_count_a;
            $oGame->$fieldB = $set_count_b;
        }

        $oGame->save();
        return false;
    }

    /**
     * get GameMain Data
     * @Route("/ResultServices/getGame",  name=getGameMainData::class)
     * @return boolen
     */
    public function getGameMainData(Request $request):JsonResponse
    {
        $prmGameId = $request->request->get('gameId');
        if(empty($prmGameId)){
            return new JsonResponse(false);
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $resServiceLogModel = new ResultserviceLogModel();

        $oGame	= SfGamesModel::findOneById($prmGameId)->row();
        $oGame['winner'] = 'team_'.$resServiceLogModel::getWinnerTeamByGame($prmGameId);

        return new JsonResponse($oGame);
    }

    /**
     * Update Game Statistik tl_sf_games
     * @Route("/ResultServices/createSignImages",  name=createSignImages::class)
     * @return boolen
     */
    public function saveGameSignImages(Request $request):JsonResponse
    {
        $prmField = $request->request->get('field');
        $prmSetId = $request->request->get('setId');
        $prmGameId = $request->request->get('gameId');
        $prmImage = $request->request->get('img');

        if(empty($prmField) or empty($prmSetId) or empty($prmGameId) or empty($prmImage))
        {
            return new JsonResponse(false);
        }

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();

        $oServiceLogModel	= $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');
        if(!$oLogEntry = $oServiceLogModel::findBy(['skey=?','id_game=?'],['sig_'.$prmField,$prmGameId])) {
            $oLogEntry = new $oServiceLogModel();
        }

        $oLogEntry->id_game = $prmGameId;
        $oLogEntry->skey    = 'sig_'.$prmField;
        $oLogEntry->svalue  = $prmImage;
        $oLogEntry->tstamp  = time();
        $oLogEntry->save();

        return new JsonResponse();
    }

    /**
     * Spielbericht PDF erstellen
     * @Route("/ResultServices/createReceipt/{gameId}",  name=createGameReceipt::class , defaults={"gameId": "NULL"})
     * @return boolen
     */
    public function createGameReceipt($gameId)
    {

        //Init Contao Framework
        $objContaoFramework = $this->container->get('contao.framework');
        $objContaoFramework->initialize();
        $oServiceLogModel = $objContaoFramework->createInstance('\Memo\ResultServiceBundle\Model\ResultserviceLogModel');

        $objUser = BackendUser::getInstance();
        $objUser->authenticate();

        if ($objUser->username == 'fako' or $objUser->isAdmin === true) {


            if (is_null($gameId)) {
                return new JsonResponse(false);
            }

            $tz = 'Europe/Zurich';
            $timestamp = time();
            $dt = new \DateTime("now", new \DateTimeZone($tz));
            $dt->setTimestamp($timestamp);

            $oGame = SfGamesModel::findById($gameId);
            $oTeamA = TeamModel::findBy(['tl_mod_team.published=?', 'tl_mod_team.id=?'], [1, $oGame->team_a]);
            $oTeamA->club = ClubModel::findByPk($oTeamA->pid);
            $oTeamB = TeamModel::findBy(['tl_mod_team.published=?', 'tl_mod_team.id=?'], [1, $oGame->team_b]);
            $oTeamB->club = ClubModel::findByPk($oTeamB->pid);

            $sFilePath = \System::getContainer()->getParameter('kernel.project_dir') . "/files";
            $sTempPath = \System::getContainer()->getParameter('kernel.project_dir') . "/tmp";

            //Update Gametable close Game
            $oGame->rsclosed = 1;
            $oGame->count = 1; //calc points
            $oGame->save();

            $pdf = new PDF();
            $prmPadding = 10;
            $pdf->AddPage();
            $pdf->SetMargins(10, 0, 10);
            $pdf->SetFont('Arial', 'B', 16);
            $pRight = $pdf->GetPageWidth() - $prmPadding;

            $pdf->setY(16);
            $pdf->Cell(40, 8, 'Spielbericht: ' . utf8_decode($oGame->round) . " (" . $oGame->game_nr . ")", 0, 1);
            $pdf->SetFont('Arial', '', 10);
            $sTeams = sprintf("%s vs. %s", $oTeamA->club->name, $oTeamB->club->name);
            $sWidth = $pdf->GetStringWidth('Team: ' . utf8_decode($sTeams)) + 1;
            $pdf->Cell($sWidth, 5, 'Team: ' . utf8_decode($sTeams), 0, 0);
            $pdf->Cell(0, 5, " | " . utf8_decode($dt->format('d.m.Y - H:i')) . " Uhr", 0);
            $pdf->setY(40);

            $iSetCount = 8;
            for ($x = 1; $x <= $iSetCount; $x++) {

                $pdf->SetFont('Arial', 'B', 10);
                $sGameA = sprintf('g_%d_%d', $x, 1);
                $sGameB = sprintf('g_%d_%d', $x, 2);
                $pdf->Cell(0, 5, 'Satz ' . $x . " | " . $oGame->$sGameA . " : " . $oGame->$sGameB);
                $pdf->Ln(5);
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(6, 5, 'A', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 10);

                $oGameLog = $oServiceLogModel->getLiveScoreByGameId($gameId, $x);

                $iCount = 0;
                for ($i = 0; $i < 30; $i++) {
                    if ($i > 0 and $i % 5 == 0) {
                        $pdf->setX($pdf->getX() + 0.2);
                    }

                    if (isset($oGameLog['a'][$i]) and $oGame->team_a == $oGameLog['a'][$i]->id_team) {
                        if ($oGameLog['a'][$i]->skey === 'point') {
                            $iCount++;
                            $pdf->Cell(6, 5, $iCount, 1, 0, 'C');
                        }
                    } else {
                        $pdf->Cell(6, 5, '', 1, 0, 'C');
                    }
                }
                $pdf->Ln(6);
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(6, 5, 'B', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 10);
                $iCount = 0;
                for ($i = 0; $i < 30; $i++) {
                    if ($i > 0 and $i % 5 == 0) {
                        $pdf->setX($pdf->getX() + 0.2);
                    }
                    if (isset($oGameLog['b'][$i]) && $oGame->team_b == $oGameLog['b'][$i]->id_team) {
                        if ($oGameLog['b'][$i]->skey == 'point') {
                            $iCount++;
                            $pdf->Cell(6, 5, $iCount, 1, 0, 'C');
                        }
                    } else {
                        $pdf->Cell(6, 5, '', 1, 0, 'C');
                    }
                }
                $pdf->Ln(10);
            }

            //Add Sign Fields
            $oGameLogSign = $oServiceLogModel->findBy(['id_game=?', "skey like 'sig_%%'"], [$gameId, null], ['order by tstamp']);
            $aSign = [];
            if($oGameLogSign) {
                foreach ($oGameLogSign as $key => $val) {
                    $aSign[$val->skey] = $val->svalue;
                }
            }

            //Add Signatures
            if(isset($aSign['sig_spielfuehrerA'])) {
                $tmpImgSpA = base64_decode(str_replace("data:image/png;base64,", "", $aSign['sig_spielfuehrerA']));
                $imgSpa = sys_get_temp_dir() . '/imgA.png';
                file_put_contents($imgSpa, $tmpImgSpA);
            }
            if(isset($aSign['sig_spielfuehrerB'])) {
                $tmpImgSpB = base64_decode(str_replace("data:image/png;base64,", "", $aSign['sig_spielfuehrerB']));
                $imgSpb = sys_get_temp_dir() . '/imgB.png';
                file_put_contents($imgSpb, $tmpImgSpB);
            }
            if(isset($aSign['sig_anschreiber'])) {
                $tmpImgAns = base64_decode(str_replace("data:image/png;base64,", "", $aSign['sig_anschreiber']));
                $imgAns = sys_get_temp_dir() . '/imgAns.png';
                file_put_contents($imgAns, $tmpImgAns);
            }
            if(isset($aSign['sig_schiedsrichter'])) {
                $tmpImgSchiri = base64_decode(str_replace("data:image/png;base64,", "", $aSign['sig_schiedsrichter']));
                $imgSch = sys_get_temp_dir() . '/simgSchiri.png';
                file_put_contents($imgSch, $tmpImgSchiri);
            }

            $pdf->Ln(4);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(50, 8, utf8_decode('Spielführer A'), '', 0);
            $pdf->setX(90);
            $pdf->Cell(50, 8, utf8_decode('Spielführer B'), '', 0);
            $pdf->Ln(8);

            $pdf->setY($pdf->getY() - 1);
            $pdf->Cell(65, 20, '', 1, 0);
            $pdf->setX(90);
            $pdf->Cell(65, 20, '', 1, 0);
            $pdf->setY($pdf->getY() + 1);
            if(isset($imgSpa)) {
                $pdf->Image($imgSpa, $prmPadding + 10, $pdf->getY() + 2, 40, 12, 'PNG');
            }
            if(isset($imgSpb)) {
                $pdf->Image($imgSpb, $prmPadding + 81 + $prmPadding, $pdf->getY() + 2, 40, 12, 'PNG');
            }

            $pdf->Ln(20);
            $pdf->Cell(50, 8, utf8_decode('Anschreiber'), '', 0);
            $pdf->setX(90);
            $pdf->Cell(50, 8, utf8_decode('Schiedsrichter'), '', 1);

            $pdf->setY($pdf->getY() - 1);
            $pdf->Cell(65, 20, '', 1, 0);
            $pdf->setX(90);
            $pdf->Cell(65, 20, '', 1, 0);
            $pdf->setY($pdf->getY() + 1);

            if(isset($imgSch)) {
                $pdf->Image($imgSch, $prmPadding + 80 + $prmPadding, $pdf->getY() + 2, 40, 12, 'PNG');
            }
            if(isset($imgAns)) {
                $pdf->Image($imgAns, $prmPadding + 10, $pdf->getY() + 2, 40, 12, 'PNG');
            }

            return new Response($pdf->Output(), 200, array(
                'Content-Type' => 'application/pdf'));
        }

    }

    function _date($format="r", $timestamp=false, $timezone=false)
    {
        $userTimezone = new DateTimeZone(!empty($timezone) ? $timezone : 'GMT');
        $gmtTimezone = new DateTimeZone('GMT');
        $myDateTime = new DateTime(($timestamp!=false?date("r",(int)$timestamp):date("r")), $gmtTimezone);
        $offset = $userTimezone->getOffset($myDateTime);
        return date($format, ($timestamp!=false?(int)$timestamp:$myDateTime->format('U')) + $offset);
    }

}

?>
