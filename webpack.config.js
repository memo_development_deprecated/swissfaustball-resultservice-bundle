require('dotenv-flow').config();

const path = require('path');
const webpack = require('webpack');
// Enable Autoprefixer
// const Autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
// Enable live reload
const LiveReloadPlugin = require('webpack-livereload-plugin');

/**
 * Configuration START
 */
const assetsDirectoryRelativePath = './src/Resources/public';
const assetsDirectoryPath = path.resolve(__dirname, assetsDirectoryRelativePath);

// Configuration END
module.exports = {
    mode: 'development',
    context: __dirname,
    // Comment out to debug the configuration
    stats: 'minimal',
    entry: {
        script: [path.resolve(__dirname,'src') + '/Resources/src/js/stopwatch.js',path.resolve(__dirname,'src') + '/Resources/src/js/scripts.js'],
        result_service_detail: path.resolve(__dirname,'src') + '/Resources/src/scss/result_service_detail.scss',
        backend: path.resolve(__dirname,'src') + '/Resources/src/scss/backend.scss',
        base: path.resolve(__dirname,'src') + '/Resources/src/scss/base.scss'
    },
    output: {
        path: assetsDirectoryPath,
        sourceMapFilename: '[file].map'
    },
    devtool: 'source-map',
    resolve: {
        alias: {
            'featherlight-gallery': path.resolve(__dirname, 'node_modules/featherlight/src/featherlight.gallery.js'),
        }
    },
    module: {
        rules: [
            {
                test: /\.(png|jpe?g|gif|ico)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            publicPath: '../img',
                            outputPath: 'img',
                        },
                    }
                ],
            },
            {
                test: /\.(ttf|eot|woff|woff2|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            publicPath: '../fonts',
                            outputPath: 'fonts',
                        },
                    },
                ]
            },

            {
                test: /.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: assetsDirectoryPath,
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    }
                ]
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ]
    },
    plugins: [
        // Remove js files for CSS only entries
        new FixStyleOnlyEntriesPlugin({
            silent: true,
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            chunkFilename: '[id].css',
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery'",
            "window.$": "jquery"
        }),
        new FriendlyErrorsWebpackPlugin(),
    ],
};

if (process.env.LIVERELOAD !== undefined) {
    module.exports.plugins.push(new LiveReloadPlugin());
}
