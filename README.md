# Swissfaustball Resultate Service Bundle

## About
Mit diesem Bundle können Resultate von Spielen mittels Mobile App direkt online erfasst werden.
Dabei werden sämtliche Daten in echtzeit getrackt. Im Frontend kann so der aktuelle Spielstand verfolgt werden.

### Features
- [x] Tablett optimierte Eingabe
- [x] Automatische Generierung Spielbericht mit Unterschriften
- [x] Echtzeitausgabe des Spielstandes im Frontend 

## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/swissfaustball-resultservice-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/swissfaustball-resultservice-bundle": "dev-master",
```

## Usage (German)

## Contribution
Bug reports and pull requests are welcome
